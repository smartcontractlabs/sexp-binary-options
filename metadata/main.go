package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/smartcontractlabs/sexp-binary-options/d"
)

// var bcdURL = flag.String("bcdurl", "https://better-call.dev/v1", "url of BCD API")
// var deployer = flag.String("deployer", "KT1Ws9UhhUYphe81A8hUeGzMny8cmrd53BeT", "address of the market deployer")

var factory = flag.String("factory", "KT1REVMff3m8cJasSiAsjL59xsa1ybPgFyV1", "address of the market factory")

var debug = flag.Bool("debug", false, "print debug information")
var verbose = flag.Bool("verbose", false, "print more information")

const contentType = "Content-Type"
const appJSON = "application/json; charset=UTF-8"

type info struct {
	Name     string `json:"name"`
	Symbol   string `json:"symbol"`
	Decimals string `json:"decimals"`
}

var placeholder = info{
	"SEXP Synth Token",
	"TOKEN",
	"18",
}

var knownContracts map[string]ContractMetadata

func main() {
	flag.Parse()
	d.Debug = *debug
	d.Verbose = *verbose

	knownContracts = make(map[string]ContractMetadata)

	// start the updater for knownContracts
	ticker := time.NewTicker(time.Minute)
	go func() {
		for {
			getMetadata(*factory, `florencenet`)
			// TODO uncomment this for mainnet
			// getMetadata(*deployer, `mainnet`)
			<-ticker.C
		}
	}()

	// serve metadata
	router := httprouter.New()
	router.GET("/", optionsHeaders(getPlaceholder))
	router.GET("/:address", optionsHeaders(getInfo))

	fmt.Println("Listening for metadata requests.")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func optionsHeaders(protected httprouter.Handle) httprouter.Handle {
	return httprouter.Handle(func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		protected(w, r, ps)
	})
}

func getInfo(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// log.Debugf("%v: %s request received\n", time.Now().UTC().Format(time.RFC3339), "custom metadata")
	address := ps.ByName("address")
	// check a list of our contracts
	m, found := knownContracts[address]
	if !found {
		// return 404 if we don't know the contract
		http.Error(w, "404 not found", http.StatusNotFound)
		return
	}
	// return metadata if found
	resp, err := json.Marshal(&m)
	if err != nil {
		fmt.Printf("%s: error: marshaling response: %v\n", time.Now().Format(time.RFC3339), err)
		http.Error(w, "500 internal server error", http.StatusInternalServerError)
		return
	}
	w.Header().Set(contentType, appJSON)
	w.Write(resp)
}

func getPlaceholder(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// log.Debugf("%v: %s request received\n", time.Now().UTC().Format(time.RFC3339), "placeholder metadata")
	// return a TZIP-12 placeholder for callers that don't have custom handling
	resp, err := json.Marshal(&placeholder)
	if err != nil {
		fmt.Printf("%s: error: marshaling response: %v\n", time.Now().Format(time.RFC3339), err)
		http.Error(w, "500 internal server error", http.StatusInternalServerError)
		return
	}
	w.Header().Set(contentType, appJSON)
	w.Write(resp)
}

// {
// 	"0":{"name":"BTC-USD 100k 20th April 2021 Long", "symbol":"BTC100L", "decimals":"18"},
// 	"1":{"name":"BTC-USD 100k 20th April 2021 Short", "symbol":"BTC100S", "decimals":"18"}
// }

type TokenMetadata struct {
	Name     string `json:"name"`
	Symbol   string `json:"symbol"`
	Decimals string `json:"decimals"`
}

type ContractMetadata map[string]TokenMetadata

func getMetadata(factory, network string) {
	// get all contracts originated by market factory
	ops, err := getDeployedMarkets(factory, network)
	if err != nil {
		log.Fatalf("getting market contracts: %v", err)
	}
	// for each contract, get the storage and parse it
	for i, op := range ops {
		c := op.OriginatedContract.Address
		// skip last element which is the origination of the factory itself
		if i == len(ops)-1 {
			continue
		}
		if _, found := knownContracts[c]; found {
			continue
		}
		storage, err := getMarketStorage(c, network)
		if err != nil {
			log.Fatalf("getting contract storage: %v", err)
		}
		targetPrice, err := strconv.Atoi(storage.TargetPrice)
		if err != nil {
			log.Fatalf("parsing target price: %v", err)
		}
		// in the market and in Harbinger prices have six decimals
		targetPrice = targetPrice / 1000000

		// create name and symbol based on asset, target price, expiration
		name := storage.Asset + ` ` + strconv.Itoa(targetPrice/1000) + `k ` + storage.TradingEnd.UTC().Format(`2 Jan 2006 15:04`)
		symbol := strings.Split(storage.Asset, `-`)[0] + strconv.Itoa(targetPrice/1000)
		m := ContractMetadata{}
		m0 := TokenMetadata{
			Name:     name + ` Long`,
			Symbol:   symbol + `L`,
			Decimals: "18",
		}
		m1 := TokenMetadata{
			Name:     name + ` Short`,
			Symbol:   symbol + `S`,
			Decimals: "18",
		}
		m["0"] = m0
		m["1"] = m1
		// remember the metadata for the market contract
		// TODO possibly don't show token metadata when queried with market contract address,
		// 		however we need to remember it anyway since we rely on market address to skip already known contracts
		// 		if we want to do this, have 2 maps: knownMarketContracts and knownOptionContracts but only provide metadata for options
		knownContracts[c] = m
		// remember the metadata for the options contract
		d.Debugf("saving metadata for %s: %v\n", storage.OptionContract, m)
		knownContracts[storage.OptionContract] = m
	}
	return
}
