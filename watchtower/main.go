package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/smartcontractlabs/sexp-binary-options/d"
	"gitlab.com/smartcontractlabs/sexp-binary-options/watchtower/w"
)

var watchtower = flag.String("watchtower", "tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1", "address of the watchtower account, tezos-client needs to have the keys")
var rpcURL = flag.String("rpcurl", "https://florencenet.smartpy.io/", "url of tezos-node rpc")

var factory = flag.String("factory", "KT1REVMff3m8cJasSiAsjL59xsa1ybPgFyV1", "address of the market factory")
var network = flag.String("network", "florencenet", "scans florencenet contracts by default, use this flag to scan mainnet instead")

var debug = flag.Bool("debug", false, "print debug information")
var verbose = flag.Bool("verbose", false, "print more information")

type userOfContract struct {
	user     string
	contract string
}

func main() {
	flag.Parse()
	d.Debug = *debug
	d.Verbose = *verbose

	d.Debugln("alice:", *watchtower)
	d.Debugln("rpcURL:", *rpcURL)

	// check for tezos-client
	if *verbose {
		fmt.Println("Checking for tezos-client.")
	}

	if _, err := os.Stat("tezos-client"); os.IsNotExist(err) {
		fmt.Println("Please navigate to the folder with your tezos-client and run the script there.")
		return
	}

	claimed := make(map[userOfContract]bool)
	exercised := make(map[userOfContract]bool)

	// run once per 30s
	ticker := time.NewTicker(time.Second * 30)

	for {
		counter, err := w.GetCounter(*rpcURL, *watchtower)
		if err != nil {
			log.Printf("getting counter for %s: %v", *watchtower, err)
			<-ticker.C
			continue
		}

		// get all contracts originated by market factory
		ops, err := getDeployedMarkets(*factory, *network)
		if err != nil {
			log.Printf("getting market contracts: %v", err)
			<-ticker.C
			continue
		}
		// for each contract, get the storage and parse it
		for i, op := range ops {
			market := op.OriginatedContract.Address
			// skip last element which is the origination of the factory itself
			if i == len(ops)-1 {
				continue
			}
			storage, err := getMarketStorage(market, *network)
			if err != nil {
				log.Printf("getting storage for %s: %v", market, err)
				<-ticker.C
				continue
			}

			if time.Now().After(storage.BiddingEnd) {
				longBids, err := getBidBigMap(storage.LongBids, *network)
				if err != nil {
					log.Printf("getting long_bids for %s: %v", market, err)
					<-ticker.C
					continue
				}
				shortBids, err := getBidBigMap(storage.ShortBids, *network)
				if err != nil {
					log.Printf("getting short_bids for %s: %v", market, err)
					<-ticker.C
					continue
				}
				// check for claimable bids via TZKT bigmap API
				// claim for all users, TODO handle cases where all users can't fit into one tx
				claimable := make(map[string]bool)
				for _, user := range longBids {
					// keys removed from big map are marked as active == false
					if user.Active {
						claimable[user.Key] = true
					}
				}
				for _, user := range shortBids {
					// keys removed from big map are marked as active == false
					if user.Active {
						claimable[user.Key] = true
					}
				}
				// deduplication of keys handled automatically by map type (bool value is not needed)
				users := []string{}
				for u := range claimable {
					if claimed[userOfContract{u, storage.OptionContract}] {
						// skip the ones we already claimed, but changes didn't show up in TZKT yet
						continue
					}
					users = append(users, u)
				}
				d.Debugln("claimed", claimed)
				if len(users) > 0 {
					d.Verboseln("Claiming: market", market, ", options", storage.OptionContract, "for:", users)

					counter++
					err = w.ClaimMany(*rpcURL, *watchtower, market, users, counter)
					if err != nil {
						log.Printf("executing claim: %v", err)
						<-ticker.C
						continue
					}
					// add all claimed to cache, don't claim again
					for _, u := range users {
						claimed[userOfContract{u, storage.OptionContract}] = true
					}
				}
			}
			if time.Now().After(storage.TradingEnd) {
				// if the winning side hasn't been decided we need to get prices from the oracle
				d.Debugln("winning side:", storage.WinningSide)
				// JSON null from TZKT response is unmarshalled into zero value, for strings it's empty string
				if storage.WinningSide == "" {
					d.Verboseln("Getting", storage.Asset, "prices for market:", market)
					counter++
					err = w.GetPrices(*rpcURL, *watchtower, market, storage.Asset, storage.Harbinger, counter)
					if err != nil {
						if strings.Contains(err.Error(), `price is outdated`) {
							// Harbinger doesn't have a fresh price yet, just try again the next time around
							continue
						}
						log.Printf("executing oracle call: %v", err)
						<-ticker.C
						continue
					}
				}

				obm, err := geOptionsBigMap(storage.OptionContract, *network)
				if err != nil {
					log.Printf("getting options bigmap: %v", err)
					<-ticker.C
					continue
				}

				// check for exerciseable option balances
				users := []string{}
				d.Debugln("obm:", obm)
				for _, u := range obm {
					// skip losing keys and zero balance keys
					if (storage.WinningSide == "0" && u.Value.Long.Balance != "0") || (storage.WinningSide == "1" && u.Value.Short.Balance != "0") {
						users = append(users, u.Key)
					}
				}

				// exercise for each user, use a list, experiment with how big of a list we can do
				if len(users) > 0 {
					d.Verboseln("Exercising: market", market, ", options", storage.OptionContract, "for:", users)

					counter++
					_, err = w.ExerciseMany(*rpcURL, *watchtower, market, storage.OptionContract, storage.Kusd, storage.WinningSide, users, counter)
					if err != nil {
						log.Printf("executing exercise: %v", err)
						<-ticker.C
						continue
					}
					// add all exercised to cache, don't exercise again
					for _, u := range users {
						exercised[userOfContract{u, storage.OptionContract}] = true
					}
				}
			}
		}
		d.Verboseln("Waiting to run again in 30s. Ctrl-C to exit.")
		<-ticker.C
	}

	// we don't need to save counter value anywhere since we get a fresh one each block
	// call loop each block or each 30 sec
}
