package w

import (
	"fmt"
	"math/big"
	"os/exec"
	"regexp"
	"strconv"
	"strings"

	"github.com/bitfield/script"
	"gitlab.com/smartcontractlabs/sexp-binary-options/d"
)

//  ./tezos-client -E https://delphinet-tezos.giganode.io rpc get https://delphinet-tezos.giganode.io/chains/main/blocks/head/context/contracts/tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1

func GetCounter(rpcURL, alice string) (int, error) {
	d.Debugln("getting counter for:", alice)
	counter, err := script.
		Exec(fmt.Sprintf("./tezos-client -E %s rpc get %schains/main/blocks/head/context/contracts/%s", rpcURL, rpcURL, alice)).
		Match("counter").
		Column(5).
		String()
	if err != nil {
		return 0, fmt.Errorf("getting counter for %s: %v", alice, err)
	}
	c, err := strconv.Atoi(strings.Trim(strings.TrimSpace(counter), `"`))
	if err != nil {
		return 0, fmt.Errorf("parsing counter: %v", err)
	}
	d.Debugln("counter:", counter)
	return c, nil
}

// ClaimMany removes user's bids from the market contract and awards options in the FA2 options contract.
func ClaimMany(rpcURL, watchtower, marketAddress string, users []string, counter int) error {
	l := buildClaimList(users)
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "transfer", "0", "from", watchtower, "to", marketAddress, "--entrypoint", "claim", "--arg", l, "--burn-cap", "3", "-C", strconv.Itoa(counter))
	b, err := c.CombinedOutput()
	if err != nil {
		// extract the FAILWITH error message
		s, e := script.
			Echo(string(b)).
			MatchRegexp(regexp.MustCompile(`^with[\s]+.+$`)).
			String()
		if e != nil {
			return fmt.Errorf("running commands: %v", string(b))
		}
		return fmt.Errorf("contract error: %v", s)
	}
	d.Debugln(string(b))
	d.Verboseln("Claim success.")
	return nil
}

// transfer 0 from tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1 to KT1LWDzd6mFhjjnb65a1PjHDNZtFKBieTQKH --entrypoint 'get' --arg 'Pair "BTC-USD" "KT1QsdRzQtgmDr9cLZviGPPSezFXKWPFh3x7%receive_prices"'

// GetPrices calls Harbinger normalizer contract and requests a price update for the market contract.
func GetPrices(rpcURL, alice, marketAddress, asset, harbinger string, counter int) error {
	c := exec.Command("./tezos-client", "-E", rpcURL, "transfer", "0", "from", alice, "to", harbinger, "--entrypoint", "get", "--arg", fmt.Sprintf(`Pair "%s" "%s%s"`, asset, marketAddress, `%receive_prices`), "--burn-cap", "3", "-C", strconv.Itoa(counter))
	b, err := c.CombinedOutput()
	if err != nil {
		// extract the FAILWITH error message
		s, e := script.
			Echo(string(b)).
			Join().
			ReplaceRegexp(regexp.MustCompile(`.*script reached FAILWITH instruction`), "").
			ReplaceRegexp(regexp.MustCompile(`(?U)^.*"`), "").
			ReplaceRegexp(regexp.MustCompile(`".*$`), "").
			String()
		if e != nil {
			return fmt.Errorf("running commands: %v", string(b))
		}
		d.Debugln(string(b))
		return fmt.Errorf("contract error: %v", s)
	}
	d.Debugln(string(b))
	d.Verboseln("Get price success.")
	return nil
}

// transfer 0 from tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1 to KT1Nxpog4bJiQzW4DfmtNXk2Hv1tZHLLCUNZ --entrypoint 'balance_of' --arg 'Pair  { Pair "tz1cKN5tR1CvfsCN2nFpzvmPdJi9z8NLMU9W" 0 ;  Pair "tz1cKN5tR1CvfsCN2nFpzvmPdJi9z8NLMU9W" 1 ;  Pair "tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1" 0 ;  Pair "tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1" 1 ;  Pair "tz1Qw7r9P5NDVm7KwAKQGFXGPpqH5RgDPYQx" 1 ;  Pair "tz1Qw7r9P5NDVm7KwAKQGFXGPpqH5RgDPYQx" 0 }  "KT1Q14x1evBgEjiZiJuZJ39Bcp9WaAEd1xAa%exercise"'
// Pair  { Pair "tz1cKN5tR1CvfsCN2nFpzvmPdJi9z8NLMU9W" 0 }  "KT1Q14x1evBgEjiZiJuZJ39Bcp9WaAEd1xAa%exercise"

// ExerciseMany redeems winning options from the FA2 contract for kUSD, for the specified users.
func ExerciseMany(rpcURL, alice, marketAddress, optionsAddress, kUSDAddress, winningSide string, users []string, counter int) (*big.Int, error) {
	l := buildExerciseList(users, winningSide)
	arg := fmt.Sprintf(`Pair %s "%s%s"`, l, marketAddress, `%exercise`)
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "transfer", "0", "from", alice, "to", optionsAddress, "--entrypoint", "balance_of", "--arg", arg, "--burn-cap", "3", "-C", strconv.Itoa(counter))
	b, err := c.CombinedOutput()
	if err != nil {
		// extract the FAILWITH error message
		s, e := script.
			Echo(string(b)).
			MatchRegexp(regexp.MustCompile(`^with[\s]+.+$`)).
			String()
		if e != nil {
			return &big.Int{}, fmt.Errorf("running commands: %v", string(b))
		}
		return &big.Int{}, fmt.Errorf("contract error: %v", s)
	}
	d.Debugln("before regex:", string(b))

	// extract parameter of the call to kUSD
	s, e := script.
		Echo(string(b)).
		Join().
		ReplaceRegexp(regexp.MustCompile(`.*Internal operations:`), "").
		ReplaceRegexp(regexp.MustCompile(fmt.Sprintf(`.*To: %s`, kUSDAddress)), "").
		ReplaceRegexp(regexp.MustCompile(`This transaction was successfully applied.*`), "").
		// Entrypoint: transfer Parameter: (Pair 0x01f18a96473713bb1b7ae96b5e9acbbdd53a93a18f00 (Pair 0x0000a26828841890d3f3a2a1d4083839c7a882fe0501 4386800000000000000))
		Column(8).
		ReplaceRegexp(regexp.MustCompile(`\)\)`), "").
		String()
	if e != nil {
		return &big.Int{}, fmt.Errorf("running commands: %v", string(b))
	}
	d.Debugf("after regex: %#v\n", s)

	paidKUSD, succ := big.NewInt(0).SetString(strings.TrimSpace(s), 10)
	if !succ {
		// TODO less fragile handling
		// return &big.Int{}, fmt.Errorf("creating paidKUSD")
		return big.NewInt(0), nil
	}
	d.Verboseln("Exercise success.")
	return paidKUSD, nil
}

func buildExerciseList(addresses []string, winningSide string) string {
	list := ""
	for _, a := range addresses {
		list = list + ` Pair "` + a + `" ` + winningSide + ` ;`
	}
	list = `{ ` + list + ` }`
	return list
}

func buildClaimList(addresses []string) string {
	list := ""
	for _, a := range addresses {
		list = list + ` "` + a + `" ;`
	}
	list = `{ ` + list + ` }`
	return list
}
