package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/smartcontractlabs/sexp-binary-options/d"
)

// https://api.florencenet.tzkt.io/v1/bigmaps/40028/keys

// getBidBigMap2 calls TZKT to get big map with long or short bids
func getBidBigMap(bigMapID int, network string) (BidBigMap, error) {
	resp, err := http.Get(fmt.Sprintf(`https://api.%s.tzkt.io/v1/bigmaps/%d/keys`, network, bigMapID))
	if err != nil {
		return BidBigMap{}, fmt.Errorf("calling TZKT: %v", err)
	}
	// d.Debugln("resp:", resp)
	if resp.StatusCode == http.StatusNotFound {
		return BidBigMap{}, fmt.Errorf("calling TZKT: not found")
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return BidBigMap{}, fmt.Errorf("reading response: %v", err)
	}
	resp.Body.Close()
	d.Debugln("resp body:", string(b))
	var m BidBigMap
	err = json.Unmarshal(b, &m)
	if err != nil {
		return BidBigMap{}, fmt.Errorf("unmarshaling response: %v", err)
	}
	d.Debugf("bid big map: %v\n", m)
	return m, nil
}

type BidBigMap []struct {
	ID         int    `json:"id"`
	Active     bool   `json:"active"`
	Hash       string `json:"hash"`
	Key        string `json:"key"`
	Value      string `json:"value"`
	FirstLevel int    `json:"firstLevel"`
	LastLevel  int    `json:"lastLevel"`
	Updates    int    `json:"updates"`
}

// https://api.florencenet.tzkt.io/v1/contracts/KT1FRmJh6hPZCSNAKsmbkgQn3L59aimtWYb7/bigmaps/options/keys

func geOptionsBigMap(optionsContract, network string) (OptionsBigMap, error) {
	resp, err := http.Get(fmt.Sprintf(`https://api.%s.tzkt.io/v1/contracts/%s/bigmaps/options/keys`, network, optionsContract))
	if err != nil {
		return OptionsBigMap{}, fmt.Errorf("calling TZKT: %v", err)
	}
	// d.Debugln("resp:", resp)
	if resp.StatusCode == http.StatusNotFound {
		return OptionsBigMap{}, fmt.Errorf("calling TZKT: not found")
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return OptionsBigMap{}, fmt.Errorf("reading response: %v", err)
	}
	resp.Body.Close()
	d.Debugln("resp body:", string(b))
	var m OptionsBigMap
	err = json.Unmarshal(b, &m)
	if err != nil {
		return OptionsBigMap{}, fmt.Errorf("unmarshaling response: %v", err)
	}
	return m, nil
}

type OptionsBigMap []struct {
	ID     int    `json:"id"`
	Active bool   `json:"active"`
	Hash   string `json:"hash"`
	Key    string `json:"key"`
	Value  struct {
		Long struct {
			Balance   string        `json:"balance"`
			Operators []interface{} `json:"operators"`
		} `json:"0"`
		Short struct {
			Balance   string        `json:"balance"`
			Operators []interface{} `json:"operators"`
		} `json:"1"`
	} `json:"value"`
	FirstLevel int `json:"firstLevel"`
	LastLevel  int `json:"lastLevel"`
	Updates    int `json:"updates"`
}

// Market Deployer KT1Ws9UhhUYphe81A8hUeGzMny8cmrd53BeT
// Market Factory KT1REVMff3m8cJasSiAsjL59xsa1ybPgFyV1 (from deployer storage)
// https://api.florencenet.tzkt.io/v1/accounts/KT1REVMff3m8cJasSiAsjL59xsa1ybPgFyV1/operations?type=origination&limit=200&sort=1

// getDeployedMarkets calls TZKT to get contracts originated by the deployer contract.
func getDeployedMarkets(factory, network string) (Deployments, error) {
	resp, err := http.Get(fmt.Sprintf(`https://api.%s.tzkt.io/v1/accounts/%s/operations?type=origination&limit=200&sort=1`, network, factory))
	if err != nil {
		return Deployments{}, fmt.Errorf("calling TZKT: %v", err)
	}
	d.Debugln("resp:", resp)
	if resp.StatusCode == http.StatusNotFound {
		return Deployments{}, fmt.Errorf("calling TZKT: not found")
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return Deployments{}, fmt.Errorf("reading response: %v", err)
	}
	resp.Body.Close()
	d.Debugln("resp body:", string(b))
	var dep Deployments
	err = json.Unmarshal(b, &dep)
	if err != nil {
		return Deployments{}, fmt.Errorf("unmarshaling response: %v", err)
	}
	return dep, nil
}

type Deployments []struct {
	Type      string    `json:"type"`
	ID        int       `json:"id"`
	Level     int       `json:"level"`
	Timestamp time.Time `json:"timestamp"`
	Block     string    `json:"block"`
	Hash      string    `json:"hash"`
	Counter   int       `json:"counter"`
	Initiator struct {
		Address string `json:"address"`
	} `json:"initiator"`
	Sender struct {
		Address string `json:"address"`
	} `json:"sender"`
	Nonce              int    `json:"nonce"`
	GasLimit           int    `json:"gasLimit"`
	GasUsed            int    `json:"gasUsed"`
	StorageLimit       int    `json:"storageLimit"`
	StorageUsed        int    `json:"storageUsed"`
	BakerFee           int    `json:"bakerFee"`
	StorageFee         int    `json:"storageFee"`
	AllocationFee      int    `json:"allocationFee"`
	ContractBalance    int    `json:"contractBalance"`
	Status             string `json:"status"`
	OriginatedContract struct {
		Kind    string `json:"kind"`
		Address string `json:"address"`
	} `json:"originatedContract"`
}

// https://api.florencenet.tzkt.io/v1/contracts/KT1FgrsTNwWzyGB56vW9koG5Y6JEssdLNNF9/storage

// getMarketStorage calls TZKT to get information about identical contracts.
func getMarketStorage(contract string, network string) (MarketStorage, error) {
	resp, err := http.Get(fmt.Sprintf(`https://api.%s.tzkt.io/v1/contracts/%s/storage`, network, contract))
	if err != nil {
		return MarketStorage{}, fmt.Errorf("calling TZKT: %v", err)
	}
	// d.Debugln("resp:", resp)
	if resp.StatusCode == http.StatusNotFound {
		return MarketStorage{}, fmt.Errorf("calling TZKT: not found")
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return MarketStorage{}, fmt.Errorf("reading response: %v", err)
	}
	var ms MarketStorage
	err = json.Unmarshal(b, &ms)
	if err != nil {
		return MarketStorage{}, fmt.Errorf("unmarshaling response: %v", err)
	}
	resp.Body.Close()
	d.Debugln("resp body:", string(b))
	return ms, nil
}

type MarketStorage struct {
	Kusd                  string    `json:"kusd"`
	Admin                 string    `json:"admin"`
	Asset                 string    `json:"asset"`
	Harbinger             string    `json:"harbinger"`
	LongBids              int       `json:"long_bids"`
	LongPrice             string    `json:"long_price"`
	ShortBids             int       `json:"short_bids"`
	SkewLimit             string    `json:"skew_limit"`
	BiddingEnd            time.Time `json:"bidding_end"`
	Initialized           bool      `json:"initialized"`
	MinCapital            string    `json:"min_capital"`
	ShortPrice            string    `json:"short_price"`
	TradingEnd            time.Time `json:"trading_end"`
	TargetPrice           string    `json:"target_price"`
	WinningSide           string    `json:"winning_side"`
	LongTokenID           string    `json:"long_token_id"`
	MarketFactory         string    `json:"market_factory"`
	ShortTokenID          string    `json:"short_token_id"`
	MarketEarnings        string    `json:"market_earnings"`
	OptionContract        string    `json:"option_contract"`
	OptionsFactory        string    `json:"options_factory"`
	TotalLongBids         string    `json:"total_long_bids"`
	CreatorEarnings       string    `json:"creator_earnings"`
	TotalShortBids        string    `json:"total_short_bids"`
	CancelFeeMultiplier   string    `json:"cancel_fee_multiplier"`
	ExerciseFeeMultiplier string    `json:"exercise_fee_multiplier"`
}

// https://api.florencenet.tzkt.io/v1/contracts/KT1FgrsTNwWzyGB56vW9koG5Y6JEssdLNNF9/storage

// getOptionsStorage calls TZKT to get information about identical contracts.
func getOptionsStorage(contract string, network string) (OptionsStorage, error) {
	resp, err := http.Get(fmt.Sprintf(`https://api.%s.tzkt.io/v1/contracts/%s/storage`, network, contract))
	if err != nil {
		return OptionsStorage{}, fmt.Errorf("calling TZKT: %v", err)
	}
	// d.Debugln("resp:", resp)
	if resp.StatusCode == http.StatusNotFound {
		return OptionsStorage{}, fmt.Errorf("calling TZKT: not found")
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return OptionsStorage{}, fmt.Errorf("reading response: %v", err)
	}
	var os OptionsStorage
	err = json.Unmarshal(b, &os)
	if err != nil {
		return OptionsStorage{}, fmt.Errorf("unmarshaling response: %v", err)
	}
	resp.Body.Close()
	d.Debugln("resp body:", string(b))
	return os, nil
}

type OptionsStorage struct {
	Options       string    `json:"options"`
	Metadata      string    `json:"metadata"`
	Expiration    time.Time `json:"expiration"`
	OptionsMarket string    `json:"options_market"`
}
