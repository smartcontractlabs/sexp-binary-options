package d

import (
	"log"
)

var Debug bool
var Verbose bool

func Debugf(format string, v ...interface{}) {
	if Debug {
		log.Printf(format, v...)
	}
}

func Debugln(v ...interface{}) {
	if Debug {
		log.Println(v...)
	}
}

func Verbosef(format string, v ...interface{}) {
	if Verbose {
		log.Printf(format, v...)
	}
}

func Verboseln(v ...interface{}) {
	if Verbose {
		log.Println(v...)
	}
}
