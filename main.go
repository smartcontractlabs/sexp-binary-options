package main

import (
	"flag"
	"fmt"
	"log"
	"math/big"
	"os"

	"gitlab.com/smartcontractlabs/sexp-binary-options/d"
	m "gitlab.com/smartcontractlabs/sexp-binary-options/market"
)

var creator = flag.String("creator", "tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1", "address of the market creator, tezos-client needs to have the keys")
var kUSD = flag.String("kusd", "KT1LWN8m9JMAzeV4nyfzpVigchr84NWGQbUw", "kUSD address")
var marketDeployer = flag.String("deployer", "KT1Ws9UhhUYphe81A8hUeGzMny8cmrd53BeT", "address of the market deployer contract")
var rpcURL = flag.String("rpcurl", "https://florencenet.smartpy.io/", "url of tezos-node rpc")

// florencenet
// Harbinger normalizer address: KT1SUP27JhX24Kvr11oUdWswk7FnCW78ZyUn
// kUSD token address: KT1LWN8m9JMAzeV4nyfzpVigchr84NWGQbUw

var debug = flag.Bool("debug", false, "print debug information")
var verbose = flag.Bool("verbose", false, "print more information")

var biddingEnd = flag.Int64("biddingEnd", 1618069731, "unix timestamp of when bidding ends")
var tradingEnd = flag.Int64("tradingEnd", 1618588131, "unix timestamp of when trading ends")
var asset = flag.String("asset", "BTC-USD", "asset code, has to be supported by Harbinger")
var targetPrice = flag.Int64("targetPrice", 60000000000, "target price for the options, six decimals")
var longBid = flag.String("longBid", "12300000000000000000", "creator's starting long bid, 18 decimals")
var shortBid = flag.String("shortBid", "14500000000000000000", "creator's starting short bid, 18 decimals")
var minCapital = flag.String("minCapital", "10000000000000000000", "the lower bound of creator's capital, 18 decimals")
var skewLimit = flag.String("skewLimit", "80000000000000000000", `maximum allowed skew of creator bids, used as x/10^20, default 80%, 
(higher bid / total bids) can't be higher than skew`)
var cancelFeeMultiplier = flag.String("cancelFeeMultiplier", "6000000000000000", "configure fee for cancelling bids, used as x/10^18")
var exerciseFeeMultiplier = flag.String("exerciseFeeMultiplier", "3000000000000000", "configure fee for exercising options, used as x/10^18")

// 10000000000000000000   10^18
//   80000000000000000000 10^20
// 14500000000000000000   10^18
// 12300000000000000000   10^18

// const suffixFilename = ".setup_test_binary_options_suffix"
// TODO save contract addresses to some kind of a log
// either numbered or with current time

func main() {
	flag.Parse()
	d.Debug = *debug
	d.Verbose = *verbose

	d.Debugln("creator:", *creator)
	d.Debugln("kUSD:", *kUSD)
	d.Debugln("rpcURL:", *rpcURL)

	// check for tezos-client
	if _, err := os.Stat("tezos-client"); os.IsNotExist(err) {
		fmt.Println("The tezos-client binary is required to run the deploy script.")
		fmt.Println("If you don't have it, you can get it from the serokell repo.")
		fmt.Println(`$ wget https://github.com/serokell/tezos-packaging/releases/latest/download/tezos-client`)
		fmt.Println(`$ chmod +x tezos-client`)
		fmt.Println("If you already have the tezos-client binary, you can copy it into this folder.")
		return
	}

	longBid, succ := big.NewInt(0).SetString(*longBid, 10)
	if !succ {
		log.Fatalf("longBid: creating BigInt")
	}
	shortBid, succ := big.NewInt(0).SetString(*shortBid, 10)
	if !succ {
		log.Fatalf("shortBid: creating BigInt")
	}
	minCapital, succ := big.NewInt(0).SetString(*minCapital, 10)
	if !succ {
		log.Fatalf("minCapital: creating BigInt")
	}
	skewLimit, succ := big.NewInt(0).SetString(*skewLimit, 10)
	if !succ {
		log.Fatalf("skewLimit: creating BigInt")
	}
	cancelFeeMultiplier, succ := big.NewInt(0).SetString(*cancelFeeMultiplier, 10)
	if !succ {
		log.Fatalf("cancelFeeMultiplier: creating BigInt")
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString(*exerciseFeeMultiplier, 10)
	if !succ {
		log.Fatalf("exerciseFeeMultiplier: creating BigInt")
	}

	market, options, err := m.Deploy(*rpcURL, *creator, *marketDeployer, *kUSD, longBid, shortBid, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, *asset, *targetPrice, *biddingEnd, *tradingEnd)
	if err != nil {
		log.Fatalf("deploying market: %v", err)
	}

	fmt.Println("Binary options market has been deployed.")
	fmt.Println("Market contract address:", market)
	fmt.Println("Options contract address:", options)
}
