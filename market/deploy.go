package market

import (
	"fmt"
	"math/big"

	"gitlab.com/smartcontractlabs/sexp-binary-options/d"
)

// Deploy is used to originate the binary market contract system. It originates the market contract and an FA2 contract that eventually holds option tokens, approves the market contract to spend kUSD on behalf of the market creator and initializes the market contract. During initialization, initial long and short bids in kUSD are drawn from creator's kUSD account and the address of the option contract is updated in the market contract.
func Deploy(rpcURL, creator, deployer, kUSD string, initialLong, initialShort, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit *big.Int, asset string, targetPrice, biddingEnd, tradingEnd int64) (string, string, error) {
	// $ ./tezos-client -E http://localhost:20000 transfer 0 from alice to market_deployer --entrypoint deploy --arg '(Pair (Pair 7000000000000000000 3000000000000000000) (Pair (Pair (Pair 6000000000000000 3000000000000000) (Pair 10000000000000000000 80000000000000000000)) (Pair (Pair "BTC-USD" 61000000000) (Pair 1614616200 1622565000))))'

	d.Debugln("rpcURL:", rpcURL)
	d.Verboseln("verbose creator:", creator)
	d.Debugln("creator:", creator)
	d.Debugln("deployer:", deployer)

	marketAlias := "market"
	optionsAlias := "options"

	approveAmount := big.NewInt(0).Add(initialLong, initialShort)

	// approve deployer in kUSD

	// TODO only reset if we are approved for non-zero amount?
	d.Verboseln("Reseting allowance in kUSD.")
	err := approveMarket(rpcURL, creator, deployer, kUSD, big.NewInt(0))
	if err != nil {
		return "", "", fmt.Errorf("reseting allowance: %v", err)
	}
	d.Verboseln("Approving deployer in kUSD.")
	err = approveMarket(rpcURL, creator, deployer, kUSD, approveAmount)
	if err != nil {
		return "", "", fmt.Errorf("approving market: %v", err)
	}

	// call deployer to originate market and options contracts and init the market contract
	// d.Verboseln("Initializing market.")
	// d.Verboseln("This configures the options contract address and the starting bids.")
	market, options, err := deployMarket(rpcURL, creator, deployer, initialLong, initialShort, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, asset, targetPrice, biddingEnd, tradingEnd)
	if err != nil {
		return "", "", fmt.Errorf("calling deployer: %v", err)
	}

	err = rememberContract(rpcURL, marketAlias, market)
	if err != nil {
		return "", "", fmt.Errorf("saving market address: %v", err)
	}
	err = rememberContract(rpcURL, optionsAlias, options)
	if err != nil {
		return "", "", fmt.Errorf("saving options address: %v", err)
	}

	return market, options, nil
}
