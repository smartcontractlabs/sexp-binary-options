package market

import (
	"encoding/json"
	"fmt"
	"math/big"
	"os/exec"
	"regexp"
	"strconv"
	"strings"

	"github.com/bitfield/script"
	"github.com/savaki/jq"
	"gitlab.com/smartcontractlabs/sexp-binary-options/d"
)

// ./tezos-client -E http://localhost:20000 originate contract sandbox_harbinger transferring 0 from alice running contracts/harbinger_normalizer.tz --init 'Pair (Pair {"BAT-USDC";"BTC-USD";"COMP-USD";"DAI-USDC";"ETH-USD";"KNC-USD";"LINK-USD";"REP-USD";"XTZ-USD";"ZRX-USD"} { Elt "BTC-USD" (Pair (Pair 43272264136 "2021-02-08T20:38:00Z") (Pair (Pair (Pair 16755 16760) (Pair { Elt 16755 932473295309790545 ; Elt 16756 436846618405210000 ; Elt 16757 798981713271200000 ; Elt 16758 543166200955337380 ; Elt 16759 1198004402814286446 ; Elt 16760 1127721571526841426 } 5037193802282665797)) (Pair (Pair 16755 16760) (Pair { Elt 16755 21578365 ; Elt 16756 10139753 ; Elt 16757 18542815 ; Elt 16758 12587860 ; Elt 16759 27570331 ; Elt 16760 25987861 } 116406985))))}) (Pair 6 "KT1NNfziS5orym8pLvp2qsTjbq2ai9H8sDSr")' --force --burn-cap 3
// 43272264136 is the price that is returned when calling %get

// OriginateHarbinger originates an instance of the normalizer contract from the Harbinger oracle system.
// We need it to retrieve prices and resolve the binary option market.
// This is used for testing with a local sandbox network.
func OriginateHarbinger(rpcURL, alice, contractAlias, code string, price, lastUpdate int64) (string, error) {
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "originate", "contract", contractAlias, "transferring", "0", "from", alice, "running", code, "--init", fmt.Sprintf(`Pair (Pair {"BTC-USD";"COMP-USD";"DAI-USDC";"ETH-USD";"KNC-USD";"LINK-USD";"REP-USD";"XTZ-USD";"ZRX-USD"} { Elt "BTC-USD" (Pair (Pair %d "%d") (Pair (Pair (Pair 16755 16760) (Pair { Elt 16755 932473295309790545 ; Elt 16756 436846618405210000 ; Elt 16757 798981713271200000 ; Elt 16758 543166200955337380 ; Elt 16759 1198004402814286446 ; Elt 16760 1127721571526841426 } 5037193802282665797)) (Pair (Pair 16755 16760) (Pair { Elt 16755 21578365 ; Elt 16756 10139753 ; Elt 16757 18542815 ; Elt 16758 12587860 ; Elt 16759 27570331 ; Elt 16760 25987861 } 116406985))))}) (Pair 6 "KT1NNfziS5orym8pLvp2qsTjbq2ai9H8sDSr")`, price, lastUpdate), "--burn-cap", "4", "--force")
	b, err := c.CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("running commands: %v", string(b))
	}
	d.Debugln(string(b))

	harbingerAddress, err := parseAddress(string(b))
	if err != nil {
		return "", fmt.Errorf("parsing address: %v", string(b))
	}
	return harbingerAddress, nil
}

// ./tezos-client -E http://localhost:20000 originate contract sandbox_kusd_mock transferring 0 from alice running contracts/fa12_mock.tz --init 'Unit' --force --burn-cap 3

// OriginateMockKUSD originates an empty contract with the FA1.2 interface.
// We can then use it when running tests instead of the real kUSD contract.
// This is used for testing with a local sandbox network.
func OriginateMockKUSD(rpcURL, alice, contractAlias, code string) (string, error) {
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "originate", "contract", contractAlias, "transferring", "0", "from", alice, "running", code, "--init", `Unit`, "--burn-cap", "4", "--force")
	b, err := c.CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("running commands: %v", string(b))
	}
	d.Debugln(string(b))

	mockAddress, err := parseAddress(string(b))
	if err != nil {
		return "", fmt.Errorf("parsing address: %v", string(b))
	}
	return mockAddress, nil
}

// $ ./tezos-client -E http://localhost:20000 originate contract ledger_factory transferring 0 from alice running contracts/ledger_factory.tz --init 'Unit' --burn-cap 4 --force

// OriginateOptionsFactory originates a factory contract that produces options contracts, when called by a market.
func OriginateOptionsFactory(rpcURL, alice, contractAlias, code string) (string, error) {
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "originate", "contract", contractAlias, "transferring", "0", "from", alice, "running", code, "--init", `Pair {} {}`, "--burn-cap", "4", "-S", "1000000", "--force")
	b, err := c.CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("running commands: %v", string(b))
	}
	d.Debugln(string(b))

	optionsFactory, err := parseAddress(string(b))
	if err != nil {
		return "", fmt.Errorf("parsing address: %v", string(b))
	}
	return optionsFactory, nil
}

// $ ./tezos-client -E http://localhost:20000 originate contract market_factory transferring 0 from alice running contracts/market_factory2.tz --init 'Pair "KT1BS7DtAetL9LsyadrJKNDrjh79TAgKsFeS" (Pair "KT1PxpHxQmjqzcyzFmoivYzDdc99HTQySMNi" "KT1PxpHxQmjqzcyzFmoivYzDdc99HTQySMNi")' --burn-cap 4 --force

// OriginateMarketFactory originates a factory contract that produces market contracts, when called by the deployer contract.
func OriginateMarketFactory(rpcURL, alice, contractAlias, code, kusd, harbinger, optionsFactory string) (string, error) {
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "originate", "contract", contractAlias, "transferring", "0", "from", alice, "running", code, "--init", fmt.Sprintf(`Pair "%s" (Pair "%s" (Pair "%s" (Pair {} {})))`, kusd, harbinger, optionsFactory), "--burn-cap", "4", "-S", "1000000", "--force")
	b, err := c.CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("running commands: %v", string(b))
	}
	d.Debugln(string(b))

	marketFactory, err := parseAddress(string(b))
	if err != nil {
		return "", fmt.Errorf("parsing address: %v", string(b))
	}
	return marketFactory, nil
}

// $ ./tezos-client -E http://localhost:20000 originate contract market_deployer transferring 0 from alice running contracts/market_factory.tz --init 'Pair "KT1V7jGAwdWqz4oxgzZZNQEu6Wkr2AA1eYKX" (Pair "KT1V7jGAwdWqz4oxgzZZNQEu6Wkr2AA1eYKX" (Pair 0 0))' --burn-cap 4 --force

// OriginateMarketDeployer originates a deployer contract, which can be called by anyone who wants to deploy a new market.
func OriginateMarketDeployer(rpcURL, alice, contractAlias, code, marketFactory, kUSD string) (string, error) {
	// the second marketFactory address is a placeholder for %creator (overwritten on first use)
	// TODO possibly use an option type for that field
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "originate", "contract", contractAlias, "transferring", "0", "from", alice, "running", code, "--init", fmt.Sprintf(`Pair "%s" (Pair "%s" (Pair "%s" (Pair %d %d)))`, marketFactory, kUSD, marketFactory, big.NewInt(0), big.NewInt(0)), "--burn-cap", "4", "-S", "1000000", "--force")
	b, err := c.CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("running commands: %v", string(b))
	}
	d.Debugln(string(b))

	deployer, err := parseAddress(string(b))
	if err != nil {
		return "", fmt.Errorf("parsing address: %v", string(b))
	}
	return deployer, nil
}

// ./../tezos-client -E http://localhost:20000 originate contract market15 transferring 0 from delphi_alice running binary_option_market.tz --init 'Pair (Pair (Pair "KT1RXpLtz22YgX24QQhxKVyKvtKZFaAVtTB9" (Pair {} {})) (Pair None (Pair 0 1))) (Pair (Pair 1612038277 1612040077) (Pair (Pair (Pair 0 0) (Pair 0 0)) (Pair (Pair (Pair None "KT1LWDzd6mFhjjnb65a1PjHDNZtFKBieTQKH") (Pair "BTC-USD" 36000000000)) (Pair (Pair "tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1" False) (Pair (Pair (Pair 0 0) (Pair 6000000000000000 3000000000000000)) (Pair 10000000000000000000 80000000000000))))))' --burn-cap 3

// TODO
func originateMarket(rpcURL, alice, contractAlias, code, kusd, harbinger, asset string, targetPrice, biddingEnd, tradingEnd int64, minCapital, skewLimit *big.Int) (string, error) {
	// TODO make fees configurable
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "originate", "contract", contractAlias, "transferring", "0", "from", alice, "running", code, "--init", fmt.Sprintf(`Pair (Pair (Pair "%s" (Pair {} {})) (Pair None (Pair 0 1))) (Pair (Pair %d %d) (Pair (Pair (Pair 0 0) (Pair 0 0)) (Pair (Pair (Pair None "%s") (Pair "%s" %d)) (Pair (Pair "%s" False) (Pair (Pair (Pair 0 0) (Pair 6000000000000000 3000000000000000)) (Pair %d %d))))))`, kusd, biddingEnd, tradingEnd, harbinger, asset, targetPrice, alice, minCapital, skewLimit), "--burn-cap", "4", "--force")
	b, err := c.CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("running commands: %v", string(b))
	}
	d.Debugln(string(b))

	marketAddress, err := parseAddress(string(b))
	if err != nil {
		return "", fmt.Errorf("parsing address: %v", string(b))
	}
	return marketAddress, nil
}

// ./../tezos-client -E http://localhost:20000 originate contract options15 transferring 0 from delphi_alice running fa2.tz --init 'Pair {} (Pair (Pair "BTC-USD" (Pair 36000000000 1612040077)) (Pair "KT1QsdRzQtgmDr9cLZviGPPSezFXKWPFh3x7" { Elt 0 (Pair 0 { Elt "decimals" 0x3138;Elt "name" 0x4254435553442033363030302030312d33302d32303231204c6f6e67;Elt "symbol" 0x42544333364c ;}) ; Elt 1 (Pair 1 { Elt "decimals" 0x3138;Elt "name" 0x4254435553442033363030302030312d33302d323032312053686f7274;Elt "symbol" 0x425443333653}) }))' --burn-cap 3

// TODO
func originateOptions(rpcURL, alice, contractAlias, code, marketAddress, asset string, targetPrice, tradingEnd int64) (string, error) {
	// TODO encode text to bytes for metadata
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "originate", "contract", contractAlias, "transferring", "0", "from", alice, "running", code, "--init", fmt.Sprintf(`Pair {} (Pair (Pair "%s" (Pair %d %d)) (Pair "%s" { Elt 0 (Pair 0 { Elt "decimals" 0x3138;Elt "name" 0x4254435553442033363030302030312d33302d32303231204c6f6e67;Elt "symbol" 0x42544333364c ;}) ; Elt 1 (Pair 1 { Elt "decimals" 0x3138;Elt "name" 0x4254435553442033363030302030312d33302d323032312053686f7274;Elt "symbol" 0x425443333653}) }))`, asset, targetPrice, tradingEnd, marketAddress), "--burn-cap", "3", "--force")
	b, err := c.CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("originating options: %v", string(b))
	}
	d.Debugln(string(b))

	optionsAddress, err := parseAddress(string(b))
	if err != nil {
		return "", fmt.Errorf("originating options: %v", string(b))
	}
	return optionsAddress, nil
}

// ./../tezos-client -E http://localhost:20000 transfer 0 from delphi_alice to delphi_kUSD --entrypoint approve --arg 'Pair "KT1Ey5yQpkP6AxggymCthKEPDbuGbWoKzTr5" 50000000000000000000' --burn-cap 0.04125

// TODO comment
func approveMarket(rpcURL, alice, marketAddress, kusd string, amount *big.Int) error {
	// approve the market contract to move alice's kUSD
	d.Debugln("amount:", amount)
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "transfer", "0", "from", alice, "to", kusd, "--entrypoint", "approve", "--arg", fmt.Sprintf(`Pair "%s" %d`, marketAddress, amount), "--burn-cap", "3")
	b, err := c.CombinedOutput()
	if err != nil {
		return fmt.Errorf("approving market: %v", string(b))
	}
	d.Debugln(string(b))
	return nil
}

// $ ./tezos-client -E http://localhost:20000 transfer 0 from alice to market_deployer --entrypoint deploy --arg '(Pair (Pair 7000000000000000000 3000000000000000000) (Pair (Pair (Pair 6000000000000000 3000000000000000) (Pair 10000000000000000000 80000000000000000000)) (Pair (Pair "BTC-USD" 61000000000) (Pair 1614616200 1622565000))))'

// TODO comment
func deployMarket(rpcURL, creator, deployer string, initialLong, initialShort, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit *big.Int, asset string, targetPrice, biddingEnd, tradingEnd int64) (string, string, error) {
	// initialize market with options address and initial bids
	d.Debugln("longBid:", initialLong)
	d.Debugln("shortBid:", initialShort)
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "transfer", "0", "from", creator, "to", deployer, "--entrypoint", "deploy", "--arg", fmt.Sprintf(`(Pair (Pair %d %d) (Pair (Pair (Pair %d %d) (Pair %d %d)) (Pair (Pair "%s" %d) (Pair %d %d))))`, initialLong, initialShort, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, asset, targetPrice, biddingEnd, tradingEnd), "--burn-cap", "5")
	b, err := c.CombinedOutput()
	if err != nil {
		// extract the FAILWITH error message
		s, e := script.
			Echo(string(b)).
			MatchRegexp(regexp.MustCompile(`^with[\s]+.+$`)).
			String()
		if e != nil {
			return "", "", fmt.Errorf("running commands: %v", string(b))
		}
		d.Debugln(string(b))
		return "", "", fmt.Errorf("contract error: %v", s)
	}
	d.Debugln(string(b))
	market, options, err := parseAddress2(string(b))
	if err != nil {
		return "", "", fmt.Errorf("parsing originated contracts: %v", err)
	}
	d.Debugln("market:", market)
	d.Debugln("options:", options)
	return market, options, nil
}

// ./../tezos-client -E http://localhost:20000 transfer 0 from delphi_alice to market13 --entrypoint init --arg 'Pair "KT1ULoDHV9GqsbnmrCmYpswZcs95cRDK4Ra9" (Pair 7200000000000000000 2800000000000000000)' --burn-cap 3

// TODO comment
func initMarket(rpcURL, alice, marketAddress string, longBid, shortBid *big.Int) (string, error) {
	// initialize market with options address and initial bids
	d.Debugln("longBid:", longBid)
	d.Debugln("shortBid:", shortBid)
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "transfer", "0", "from", alice, "to", marketAddress, "--entrypoint", "init", "--arg", fmt.Sprintf(`Pair %d %d`, longBid, shortBid), "--burn-cap", "3")
	b, err := c.CombinedOutput()
	if err != nil {
		// extract the FAILWITH error message
		s, e := script.
			Echo(string(b)).
			MatchRegexp(regexp.MustCompile(`^with[\s]+.+$`)).
			String()
		if e != nil {
			return "", fmt.Errorf("running commands: %v", string(b))
		}
		return "", fmt.Errorf("contract error: %v", s)
	}
	d.Debugln(string(b))
	optionsAddress, err := parseAddress(string(b))
	if err != nil {
		return "", fmt.Errorf("originating options: %v", string(b))
	}
	return optionsAddress, nil
}

// ./../tezos-client -E http://localhost:20000 transfer 0 from delphi_clive to market15 --entrypoint bid --arg 'Pair False 23400000000000000000' --burn-cap 0.04125

// Bid makes a bid in the binary options market.
// Set side to true for long bids and false for short bids.
func Bid(rpcURL, bidder, market string, amount *big.Int, side bool) error {
	d.Debugln("amount:", amount)
	var sideStr string
	if side {
		sideStr = "True"
	} else {
		sideStr = "False"
	}
	d.Debugln("side:", sideStr)
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "transfer", "0", "from", bidder, "to", market, "--entrypoint", "bid", "--arg", fmt.Sprintf(`Pair %s %d`, sideStr, amount), "--burn-cap", "3")
	b, err := c.CombinedOutput()
	if err != nil {
		// extract the FAILWITH error message
		s, e := script.
			Echo(string(b)).
			MatchRegexp(regexp.MustCompile(`^with[\s]+.+$`)).
			String()
		if e != nil {
			return fmt.Errorf("running commands: %v", string(b))
		}
		return fmt.Errorf("contract error: %v", s)
	}
	d.Debugln(string(b))
	return nil
}

// ./../tezos-client -E http://localhost:20000 transfer 0 from delphi_clive to market15 --entrypoint cancel --arg 'Pair False 23400000000000000000' --burn-cap 0.04125

// Cancel cancels or reduces a bid in the binary options market.
// Set side to true for long bids and false for short bids.
func Cancel(rpcURL, bidder, market string, amount *big.Int, side bool) error {
	d.Debugln("amount:", amount)
	var sideStr string
	if side {
		sideStr = "True"
	} else {
		sideStr = "False"
	}
	d.Debugln("side:", sideStr)
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "transfer", "0", "from", bidder, "to", market, "--entrypoint", "cancel", "--arg", fmt.Sprintf(`Pair %s %d`, sideStr, amount), "--burn-cap", "3")
	b, err := c.CombinedOutput()
	if err != nil {
		// extract the FAILWITH error message
		s, e := script.
			Echo(string(b)).
			MatchRegexp(regexp.MustCompile(`^with[\s]+.+$`)).
			String()
		if e != nil {
			return fmt.Errorf("running commands: %v", string(b))
		}
		return fmt.Errorf("contract error: %v", s)
	}
	d.Debugln(string(b))
	return nil
}

// Claim removes user's bids from the market contract and awards options in the FA2 options contract.
func Claim(rpcURL, watchtower, marketAddress, user string) error {
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "transfer", "0", "from", watchtower, "to", marketAddress, "--entrypoint", "claim", "--arg", fmt.Sprintf(`{"%s"}`, user), "--burn-cap", "3")
	b, err := c.CombinedOutput()
	if err != nil {
		// extract the FAILWITH error message
		s, e := script.
			Echo(string(b)).
			MatchRegexp(regexp.MustCompile(`^with[\s]+.+$`)).
			String()
		if e != nil {
			return fmt.Errorf("running commands: %v", string(b))
		}
		return fmt.Errorf("contract error: %v", s)
	}
	d.Debugln(string(b))
	return nil
}

// transfer 0 from tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1 to KT1Nxpog4bJiQzW4DfmtNXk2Hv1tZHLLCUNZ --entrypoint 'balance_of' --arg 'Pair  { Pair "tz1cKN5tR1CvfsCN2nFpzvmPdJi9z8NLMU9W" 0 ;  Pair "tz1cKN5tR1CvfsCN2nFpzvmPdJi9z8NLMU9W" 1 ;  Pair "tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1" 0 ;  Pair "tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1" 1 ;  Pair "tz1Qw7r9P5NDVm7KwAKQGFXGPpqH5RgDPYQx" 1 ;  Pair "tz1Qw7r9P5NDVm7KwAKQGFXGPpqH5RgDPYQx" 0 }  "KT1Q14x1evBgEjiZiJuZJ39Bcp9WaAEd1xAa%exercise"'
// Pair  { Pair "tz1cKN5tR1CvfsCN2nFpzvmPdJi9z8NLMU9W" 0 }  "KT1Q14x1evBgEjiZiJuZJ39Bcp9WaAEd1xAa%exercise"

// Exercise redeems winning options from the FA2 contract for kUSD, for the specified users.
func Exercise(rpcURL, alice, marketAddress, optionsAddress, kUSDAddress string, users []string, winningSide bool) (*big.Int, error) {
	if len(users) == 0 {
		return &big.Int{}, nil
	}
	l := buildExerciseList(users, winningSide)
	arg := fmt.Sprintf(`Pair %s "%s%s"`, l, marketAddress, `%exercise`)
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "transfer", "0", "from", alice, "to", optionsAddress, "--entrypoint", "balance_of", "--arg", arg, "--burn-cap", "3")
	b, err := c.CombinedOutput()
	// d.Debugln("balance_of call:", string(b))
	if err != nil {
		// extract the FAILWITH error message
		s, e := script.
			Echo(string(b)).
			MatchRegexp(regexp.MustCompile(`^with[\s]+.+$`)).
			String()
		if e != nil {
			return &big.Int{}, fmt.Errorf("running commands: %v", string(b))
		}
		return &big.Int{}, fmt.Errorf("contract error: %v", s)
	}
	d.Debugln("before regex:", string(b))

	// extract parameter of the call to kUSD
	s, e := script.
		Echo(string(b)).
		Join().
		ReplaceRegexp(regexp.MustCompile(`.*Internal operations:`), "").
		ReplaceRegexp(regexp.MustCompile(fmt.Sprintf(`.*To: %s`, kUSDAddress)), "").
		ReplaceRegexp(regexp.MustCompile(`This transaction was successfully applied.*`), "").
		// Entrypoint: transfer Parameter: (Pair 0x01f18a96473713bb1b7ae96b5e9acbbdd53a93a18f00 (Pair 0x0000a26828841890d3f3a2a1d4083839c7a882fe0501 4386800000000000000))
		Column(8).
		ReplaceRegexp(regexp.MustCompile(`\)\)`), "").
		String()
	if e != nil {
		return &big.Int{}, fmt.Errorf("running commands: %v", string(b))
	}
	d.Debugf("after regex: %#v\n", s)

	paidKUSD, succ := big.NewInt(0).SetString(strings.TrimSpace(s), 10)
	if !succ {
		// TODO less fragile handling
		// return &big.Int{}, fmt.Errorf("creating paidKUSD")
		return big.NewInt(0), nil
	}
	return paidKUSD, nil
}

func buildExerciseList(addresses []string, winningSide bool) string {
	var winningSideInt int
	if winningSide {
		winningSideInt = 0
	} else {
		winningSideInt = 1
	}
	list := ""
	for _, a := range addresses {
		list = list + ` Pair "` + a + `" ` + strconv.Itoa(winningSideInt) + ` ;`
	}
	list = `{ ` + list + ` }`
	return list
}

// WithdrawProfit withdraws market creator's profit from the market contract.
func WithdrawProfit(rpcURL, creator, marketAddress, kUSDAddress string) (*big.Int, error) {
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "transfer", "0", "from", creator, "to", marketAddress, "--entrypoint", "withdraw_profit", "--burn-cap", "3")
	b, err := c.CombinedOutput()
	if err != nil {
		// extract the FAILWITH error message
		s, e := script.
			Echo(string(b)).
			MatchRegexp(regexp.MustCompile(`^with[\s]+.+$`)).
			String()
		if e != nil {
			return &big.Int{}, fmt.Errorf("running commands: %v", string(b))
		}
		return &big.Int{}, fmt.Errorf("contract error: %v", s)
	}
	d.Debugln("before regex:", string(b))

	// extract parameter of the call to kUSD
	s, e := script.
		Echo(string(b)).
		Join().
		ReplaceRegexp(regexp.MustCompile(`.*Internal operations:`), "").
		ReplaceRegexp(regexp.MustCompile(fmt.Sprintf(`.*To: %s`, kUSDAddress)), "").
		ReplaceRegexp(regexp.MustCompile(`This transaction was successfully applied.*`), "").
		// Entrypoint: transfer Parameter: (Pair 0x01f18a96473713bb1b7ae96b5e9acbbdd53a93a18f00 (Pair 0x0000a26828841890d3f3a2a1d4083839c7a882fe0501 4386800000000000000))
		Column(8).
		ReplaceRegexp(regexp.MustCompile(`\)\)`), "").
		String()
	if e != nil {
		return &big.Int{}, fmt.Errorf("running commands: %v", string(b))
	}
	d.Debugf("after regex: %#v\n", s)

	paidKUSD, succ := big.NewInt(0).SetString(strings.TrimSpace(s), 10)
	if !succ {
		// TODO less fragile handling
		// return &big.Int{}, fmt.Errorf("creating paidKUSD")
		return big.NewInt(0), nil
	}
	return paidKUSD, nil
}

// transfer 0 from tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1 to KT1LWDzd6mFhjjnb65a1PjHDNZtFKBieTQKH --entrypoint 'get' --arg 'Pair "BTC-USD" "KT1QsdRzQtgmDr9cLZviGPPSezFXKWPFh3x7%receive_prices"'

// GetPrices calls Harbinger normalizer contract and requests a price update for the market contract.
func GetPrices(rpcURL, alice, marketAddress, asset, harbinger string) error {
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "transfer", "0", "from", alice, "to", harbinger, "--entrypoint", "get", "--arg", fmt.Sprintf(`Pair "%s" "%s%s"`, asset, marketAddress, `%receive_prices`), "--burn-cap", "3")
	b, err := c.CombinedOutput()
	if err != nil {
		// extract the FAILWITH error message
		s, e := script.
			Echo(string(b)).
			// Harbinger doesn't return strings so we have to relax the regexp a bit
			MatchRegexp(regexp.MustCompile(`^with[\s]+.+$`)).
			String()
		if e != nil {
			return fmt.Errorf("running commands: %v", string(b))
		}
		d.Debugln(string(b))
		d.Debugln(s)
		return fmt.Errorf("contract error: %v", s)
	}
	d.Debugln(string(b))
	return nil
}

// TransferOptions transfers user's option tokens to destination address.
func TransferOptions(rpcURL, user, destination, optionsAddress string, tokenID int, amount *big.Int) error {
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "transfer", "0", "from", user, "to", optionsAddress, "--entrypoint", "transfer", "--arg", fmt.Sprintf(`{ Pair "%s" { Pair "%s" (Pair %d %s)} }`, user, destination, tokenID, amount.String()), "--burn-cap", "3")
	b, err := c.CombinedOutput()
	if err != nil {
		// extract the FAILWITH error message
		s, e := script.
			Echo(string(b)).
			MatchRegexp(regexp.MustCompile(`^with[\s]+.+$`)).
			String()
		if e != nil {
			return fmt.Errorf("running commands: %v", string(b))
		}
		return fmt.Errorf("contract error: %v", s)
	}
	d.Debugln(string(b))
	return nil
}

func parseAddress(receipt string) (string, error) {
	lines := strings.Split(strings.TrimSpace(receipt), "\n")
	for _, l := range lines {
		if strings.Contains(l, "New contract") {
			parts := strings.Split(strings.TrimSpace(l), " ")
			// ["New", "contract", "KT1ScpCcTB4K9HbTDCe8Z5FjArJT5GEyyrK1", "originated."]
			for _, p := range parts {
				// trim whitespace just in case
				p = strings.TrimSpace(p)
				if strings.HasPrefix(p, "KT1") {
					return p, nil
				}
			}
		}
	}
	return "", fmt.Errorf("no address detected")
}

func parseAddress2(receipt string) (string, string, error) {
	contracts := []string{}
	lines := strings.Split(strings.TrimSpace(receipt), "\n")
	for _, l := range lines {
		if strings.Contains(l, "New contract") {
			parts := strings.Split(strings.TrimSpace(l), " ")
			// ["New", "contract", "KT1ScpCcTB4K9HbTDCe8Z5FjArJT5GEyyrK1", "originated."]
			for _, p := range parts {
				// trim whitespace just in case
				p = strings.TrimSpace(p)
				if strings.HasPrefix(p, "KT1") {
					contracts = append(contracts, p)
				}
			}
		}
	}
	if len(contracts) != 2 {
		return "", "", fmt.Errorf("no address detected")
	}
	return contracts[0], contracts[1], nil
}

// GetBigMapElem uses tezos-client to retrieve a value for some key in the specified big map.
// I'm not sure how to properly get errors from tezos-client to show up when using the pipe methods.
func GetBigMapElem(rpcURL, key string, bigMapID int) (*big.Int, error) {
	// ./tezos-client -E http://localhost:20000 hash data '"tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"' of type address
	expr, err := script.
		Exec(fmt.Sprintf(`./tezos-client-quiet -E %s hash data '"%s"' of type address`, rpcURL, key)).
		Match("Script-expression-ID-Hash: expr").
		Column(2).
		String()
	if err != nil {
		return &big.Int{}, fmt.Errorf("hashing key %s: %v", key, err)
	}
	expr = strings.TrimSpace(expr)
	d.Debugf("key expr hash: %#v\n", expr)
	// rpc get /chains/main/blocks/head/context/big_maps/23/exprtr3iA2ZhFDtnJZDS1nVxJYeXGWw2AWziVAD7DZf7kxsHmNLZBB
	v, err := script.
		Exec(fmt.Sprintf("./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/big_maps/%d/%s", rpcURL, bigMapID, expr)).
		String()
	if err != nil {
		p := script.Exec(fmt.Sprintf("./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/big_maps/%d/%s", rpcURL, bigMapID, expr))
		p.SetError(nil)
		v, err := p.String()
		// d.Debugf("v2: %#v\n", v)
		// d.Debugf("err2: %#v\n", err)
		if strings.Contains(v, "Did not find service") {
			// this actually means invalid key, I think
			return &big.Int{}, fmt.Errorf("invalid key: %v expr: %v", key, expr)
		}
		if strings.Contains(v, "No service found at this URL") {
			// field not found
			return &big.Int{}, fmt.Errorf("no value for %s in bigmap: %v", key, bigMapID)
		}
		return &big.Int{}, fmt.Errorf("getting value for %s of bigmap %d: %v %v", key, bigMapID, v, err)
	}
	// d.Debugln("v:", v)
	// d.Debugln("err:", err)
	pattern := jq.Must(jq.Parse(".int"))
	value, err := getNat(pattern, json.RawMessage(v))
	if err != nil {
		return &big.Int{}, fmt.Errorf("getting value for %s of bigmap %d: %v", key, bigMapID, err)
	}
	d.Debugln("value:", value)
	return value, nil
}

// GetOptionsElem uses tezos-client to retrieve the balances of option tokens for some user.
func GetOptionsElem(rpcURL, key string, bigMapID int) (*big.Int, *big.Int, error) {
	// ./tezos-client -E http://localhost:20000 hash data '"tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"' of type address
	cmd := fmt.Sprintf(`./tezos-client-quiet -E %s hash data '"%s"' of type address`, rpcURL, key)
	expr, err := script.
		Exec(cmd).
		Match("Script-expression-ID-Hash: expr").
		Column(2).
		String()
	if err != nil {
		return &big.Int{}, &big.Int{}, fmt.Errorf("hashing key %s: %v", key, err)
	}
	expr = strings.TrimSpace(expr)
	d.Debugf("key expr hash: %#v\n", expr)

	// rpc get /chains/main/blocks/head/context/big_maps/23/exprtr3iA2ZhFDtnJZDS1nVxJYeXGWw2AWziVAD7DZf7kxsHmNLZBB
	v, err := script.
		Exec(fmt.Sprintf("./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/big_maps/%d/%s", rpcURL, bigMapID, expr)).
		String()
	// d.Debugf("v1: %#v\n", v)
	// d.Debugf("err1: %#v\n", err)
	if err != nil {
		p := script.Exec(fmt.Sprintf("./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/big_maps/%d/%s", rpcURL, bigMapID, expr))
		p.SetError(nil)
		v, err := p.String()
		// d.Debugf("v2: %#v\n", v)
		// d.Debugf("err2: %#v\n", err)
		if strings.Contains(v, "Did not find service") {
			// this actually means invalid key, I think
			return &big.Int{}, &big.Int{}, fmt.Errorf("invalid key: %v expr: %v", key, expr)
		}
		if strings.Contains(v, "No service found at this URL") {
			// field not found
			return &big.Int{}, &big.Int{}, fmt.Errorf("no value for %s in bigmap: %v", key, bigMapID)
		}
		return &big.Int{}, &big.Int{}, fmt.Errorf("getting value for %s of bigmap %d: %v %v", key, bigMapID, v, err)
	}
	// d.Debugf("v3: %#v\n", v)
	v = strings.TrimSpace(v)
	// d.Debugf("v4: %#v\n", v)

	// longOption .[0].args[1].args[0].int
	// shortOption .[1].args[1].args[0].int
	patternLong := jq.Must(jq.Parse(".[0].args.[1].args.[0].int"))
	patternShort := jq.Must(jq.Parse(".[1].args.[1].args.[0].int"))
	longTokens, err := getNat(patternLong, json.RawMessage(v))
	if err != nil {
		return &big.Int{}, &big.Int{}, fmt.Errorf("getting long tokens for %s of bigmap %d: %v", key, bigMapID, err)
	}
	shortTokens, err := getNat(patternShort, json.RawMessage(v))
	if err != nil {
		return &big.Int{}, &big.Int{}, fmt.Errorf("getting short tokens for %s of bigmap %d: %v", key, bigMapID, err)
	}
	d.Debugln("long tokens:", longTokens)
	d.Debugln("short tokens:", shortTokens)
	return longTokens, shortTokens, nil
}

// GetBidsMapID uses tezos-client to query storage of the market contract and retrieves the ID of long or short bids big map.
func GetBidsMapID(rpcURL, market string, side bool) (int, error) {
	var sideStr string
	var sideInt int
	if side {
		sideStr = "long"
		sideInt = 0
	} else {
		sideStr = "short"
		sideInt = 1
	}
	s, err := script.
		Exec(fmt.Sprintf(`./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/contracts/%s/storage`, rpcURL, market)).
		String()
	if err != nil {
		p := script.Exec(fmt.Sprintf(`./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/contracts/%s/storage`, rpcURL, market))
		p.SetError(nil)
		v, err := p.MatchRegexp(regexp.MustCompile("Did not find service")).
			String()
		d.Debugf("v: %#v\n", v)
		if err == nil {
			// I think this means invalid contract hash
			return 0, fmt.Errorf("invalid contract hash: %v", market)
		}
		if strings.Contains(s, "No service found at this URL") {
			// contract not found
			return 0, fmt.Errorf("no contract in context: %v", market)
		}
		return 0, fmt.Errorf("getting %s bids bigmap ID for %s: %v", sideStr, market, err)
	}
	// d.Debugf("s: %#v\n", s)
	// pattern := jq.Must(jq.Parse(fmt.Sprintf(".args.[0].args.[0].args.[1].args.[%d].int", sideInt)))
	pattern := jq.Must(jq.Parse(fmt.Sprintf(".args.[0].args.[0].args.[%d].int", sideInt+1)))
	bigMapID, err := getBigMapID(pattern, json.RawMessage(s))
	if err != nil {
		return 0, fmt.Errorf("getting %s bids bigmap ID for %s: %v", sideStr, market, err)
	}
	return bigMapID, nil
}

// GetOptionsMapID uses tezos-client to query storage of the FA2 options contract and retrieves the ID of its big map.
func GetOptionsMapID(rpcURL, options string) (int, error) {
	s, err := script.
		Exec(fmt.Sprintf(`./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/contracts/%s/storage`, rpcURL, options)).
		String()
	if err != nil {
		p := script.Exec(fmt.Sprintf(`./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/contracts/%s/storage`, rpcURL, options))
		p.SetError(nil)
		v, err := p.MatchRegexp(regexp.MustCompile("Did not find service")).
			String()
		d.Debugf("v: %#v\n", v)
		if err == nil {
			// I think this means invalid contract hash
			return 0, fmt.Errorf("invalid contract hash: %v", options)
		}
		if strings.Contains(s, "No service found at this URL") {
			// contract not found
			return 0, fmt.Errorf("no contract in context: %v", options)
		}
		return 0, fmt.Errorf("getting options bigmap ID for %s: %v", options, err)
	}
	pattern := jq.Must(jq.Parse(".args.[0].int"))
	bigMapID, err := getBigMapID(pattern, json.RawMessage(s))
	if err != nil {
		return 0, fmt.Errorf("getting options bigmap ID for %s: %v", options, err)
	}
	return bigMapID, nil
}

func getNat(jqPattern jq.Op, s json.RawMessage) (*big.Int, error) {
	v, err := jqPattern.Apply(s)
	if err != nil {
		return &big.Int{}, fmt.Errorf("accesing nat value: %v", err)
	}
	d.Debugln("v:", string(v))
	str := strings.Trim(string(v), `"`)
	value, succ := big.NewInt(0).SetString(str, 10)
	if !succ {
		return &big.Int{}, fmt.Errorf("creating bigInt")
	}
	d.Debugln("value:", value)
	return value, nil
}

// this is identical to parseWinningSide2, TODO rework both into parseInt
func getBigMapID(jqPattern jq.Op, s json.RawMessage) (int, error) {
	v, err := jqPattern.Apply(s)
	if err != nil {
		return 0, fmt.Errorf("accesing map ID: %v", err)
	}
	d.Debugln("v:", string(v))
	str := strings.Trim(string(v), `"`)
	i, err := strconv.Atoi(str)
	if err != nil {
		return 0, fmt.Errorf("parsing map ID: %v", err)
	}
	d.Debugln("i:", i)
	return i, nil
}

// GetMarketEarnings uses tezos-client to query market storage for market earnings.
func GetMarketEarnings(rpcURL, market string) (*big.Int, error) {
	s, err := script.
		Exec(fmt.Sprintf(`./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/contracts/%s/storage`, rpcURL, market)).
		String()
	if err != nil {
		p := script.Exec(fmt.Sprintf(`./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/contracts/%s/storage`, rpcURL, market))
		p.SetError(nil)
		v, err := p.MatchRegexp(regexp.MustCompile("Did not find service")).
			String()
		d.Debugf("v: %#v\n", v)
		if err == nil {
			// I think this means invalid contract hash
			return &big.Int{}, fmt.Errorf("invalid contract hash: %v", market)
		}
		if strings.Contains(s, "No service found at this URL") {
			// contract not found
			return &big.Int{}, fmt.Errorf("no contract in context: %v", market)
		}
		return &big.Int{}, fmt.Errorf("getting market earnings for %s: %v", market, err)
	}
	pattern := jq.Must(jq.Parse(".args.[5].args.[0].args.[0].int"))
	earnings, err := getNat(pattern, json.RawMessage(s))
	if err != nil {
		return &big.Int{}, fmt.Errorf("getting market earnings for %s: %v", market, err)
	}
	return earnings, nil
}

// GetCreatorEarnings uses tezos-client to query market storage for creator earnings.
func GetCreatorEarnings(rpcURL, market string) (*big.Int, error) {
	s, err := script.
		Exec(fmt.Sprintf(`./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/contracts/%s/storage`, rpcURL, market)).
		String()
	if err != nil {
		p := script.Exec(fmt.Sprintf(`./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/contracts/%s/storage`, rpcURL, market))
		p.SetError(nil)
		v, err := p.MatchRegexp(regexp.MustCompile("Did not find service")).
			String()
		d.Debugf("v: %#v\n", v)
		if err == nil {
			// I think this means invalid contract hash
			return &big.Int{}, fmt.Errorf("invalid contract hash: %v", market)
		}
		if strings.Contains(s, "No service found at this URL") {
			// contract not found
			return &big.Int{}, fmt.Errorf("no contract in context: %v", market)
		}
		return &big.Int{}, fmt.Errorf("getting creator earnings for %s: %v", market, err)
	}
	pattern := jq.Must(jq.Parse(".args.[5].args.[0].args.[1].int"))
	earnings, err := getNat(pattern, json.RawMessage(s))
	if err != nil {
		return &big.Int{}, fmt.Errorf("getting creator earnings for %s: %v", market, err)
	}
	return earnings, nil
}

// GetCancelFeeMultiplier uses tezos-client to query market storage for cancel fee multiplier.
func GetCancelFeeMultiplier(rpcURL, market string) (*big.Int, error) {
	s, err := script.
		Exec(fmt.Sprintf(`./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/contracts/%s/storage`, rpcURL, market)).
		String()
	if err != nil {
		p := script.Exec(fmt.Sprintf(`./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/contracts/%s/storage`, rpcURL, market))
		p.SetError(nil)
		v, err := p.MatchRegexp(regexp.MustCompile("Did not find service")).
			String()
		d.Debugf("v: %#v\n", v)
		if err == nil {
			// I think this means invalid contract hash
			return &big.Int{}, fmt.Errorf("invalid contract hash: %v", market)
		}
		if strings.Contains(s, "No service found at this URL") {
			// contract not found
			return &big.Int{}, fmt.Errorf("no contract in context: %v", market)
		}
		return &big.Int{}, fmt.Errorf("getting cancel fee multiplier for %s: %v", market, err)
	}
	pattern := jq.Must(jq.Parse(".args.[5].args.[1].int"))
	earnings, err := getNat(pattern, json.RawMessage(s))
	if err != nil {
		return &big.Int{}, fmt.Errorf("getting cancel fee multiplier for %s: %v", market, err)
	}
	return earnings, nil
}

func parseWinningSide1(jqPattern jq.Op, s json.RawMessage) (string, error) {
	v, err := jqPattern.Apply(s)
	if err != nil {
		return "", fmt.Errorf("accesing value: %v", err)
	}
	d.Debugln("v:", string(v))
	str := strings.Trim(string(v), `"`)
	d.Debugln("v:", str)
	return str, nil
}

func parseWinningSide2(jqPattern jq.Op, s json.RawMessage) (int, error) {
	v, err := jqPattern.Apply(s)
	if err != nil {
		return 0, fmt.Errorf("accesing value: %v", err)
	}
	d.Debugln("v:", string(v))
	str := strings.Trim(string(v), `"`)
	i, err := strconv.Atoi(str)
	if err != nil {
		return 0, fmt.Errorf("parsing value: %v", err)
	}
	d.Debugln("i:", i)
	return i, nil
}

// GetWinningSide uses tezos-client to query storage of the market contract and retrieves the winning side.
func GetWinningSide(rpcURL, market string) (bool, error) {
	s, err := script.
		Exec(fmt.Sprintf(`./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/contracts/%s/storage`, rpcURL, market)).
		String()
	if err != nil {
		p := script.Exec(fmt.Sprintf(`./tezos-client-quiet -E %s rpc get /chains/main/blocks/head/context/contracts/%s/storage`, rpcURL, market))
		p.SetError(nil)
		v, err := p.MatchRegexp(regexp.MustCompile("Did not find service")).
			String()
		d.Debugf("v: %#v\n", v)
		if err == nil {
			// I think this means invalid contract hash
			return true, fmt.Errorf("invalid contract hash: %v", market)
		}
		if strings.Contains(s, "No service found at this URL") {
			// contract not found
			return true, fmt.Errorf("no contract in context: %v", market)
		}
		return true, fmt.Errorf("getting winning side for %s: %v", market, err)
	}
	pattern := jq.Must(jq.Parse(".args.[3].args.[0].args.[0].prim"))
	decided, err := parseWinningSide1(pattern, json.RawMessage(s))
	if err != nil {
		return true, fmt.Errorf("getting winning side for %s: %v", market, err)
	}
	if decided == "None" {
		return true, fmt.Errorf("getting winning side for %s: not yet decided", market)
	}
	pattern2 := jq.Must(jq.Parse(".args.[3].args.[0].args.[0].args.[0].int"))
	winningSideInt, err := parseWinningSide2(pattern2, json.RawMessage(s))
	if err != nil {
		return true, fmt.Errorf("getting winning side for %s: %v", market, err)
	}
	if winningSideInt == 0 {
		return true, nil
	}
	if winningSideInt == 1 {
		return false, nil
	}
	return true, fmt.Errorf("getting winning side for %s: unknown side", market)
}

func configReset() error {
	d.Debugln("reseting tezos-client config")
	c := exec.Command("./tezos-client-quiet", "config", "reset")
	b, err := c.CombinedOutput()
	if err != nil {
		return fmt.Errorf("running commands to get public key: %v", string(b))
	}
	d.Debugln(string(b))
	return nil
}

// $ ./tezos-client -E http://localhost:20000 remember contract options KT1WX3Vg84KT12WLgvGFzFpRw4bL8zp8DrFs --force

func rememberContract(rpcURL, alias, address string) error {
	c := exec.Command("./tezos-client-quiet", "-E", rpcURL, "remember", "contract", alias, address, "--force")
	b, err := c.CombinedOutput()
	if err != nil {
		return fmt.Errorf("running commands to remember contract: %v", string(b))
	}
	d.Debugln(string(b))
	return nil
}
