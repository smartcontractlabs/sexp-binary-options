package market_test

import (
	"flag"
	"fmt"
	"log"
	"math/big"
	"os"
	"strings"
	"testing"
	"time"

	"gitlab.com/smartcontractlabs/sexp-binary-options/d"
	m "gitlab.com/smartcontractlabs/sexp-binary-options/market"
)

// test preparation
// ✗ tezos-client precludes running in parallel
// ✓ just run sequentially and use the bootstrap account only
//
// ✓ originate kUSD mock contract
// ✓ originate market & options, approve market in kUSD, init market
//
// test scenarios
// ✓ init & test that bid values after init are correct
// ✓ init with trading end before bidding end, should fail
// ✓ init with bids that violate skew limit
// ✓ init with not enough capital
// ✓ bid in the market, test that new value is correct
// ✓ cancel bids in the market, test that new value is correct
// ✓ bid after bidding end, should fail
// ✓ cancel after bidding end, should fail
// ✓ claim after bidding end, test that bids are removed
// ✓ claim after bidding end, test that minted option values are correct
// ✓ claim before bidding end, should fail
// ✓ claim after trading end, should succeed
// ✓ claim after cancel, check that market profit is correct
// ✓ claim after cancel, check that prices improved correctly
// ✓ exercise after trading end, test that options are removed
// ✓ exercise after trading end, test that awarded kusd values are correct
// ✓ exercise before bidding end, should fail
// ✓ exercise before trading end, should fail
// ✓ exercise, check that creator profit is correct
// ✓ withdraw_profits tests (check that profit withdraw tx sent to kUSD mock has correct kUSD amount)
// ✓ as a user, transfer options after trading end, should fail
//
// TODO
// claim & exercise for many addresses

// ✓ for tests with Harbinger, make a Harbinger mock and originate it
// ✓ receive_prices tests

var marketDeployerCode = flag.String("marketDeployerCode", "contracts/deployer/market_deployer.tz", "filename of market contract")
var marketFactoryCode = flag.String("marketFactoryCode", "contracts/deployer/market_factory.tz", "filename of market contract")
var optionsFactoryCode = flag.String("optionsFactoryCode", "contracts/deployer/ledger_factory.tz", "filename of options FA2 contract")
var kUSDMockCode = flag.String("kUSDMockCode", "contracts/fa12_mock.tz", "filename of kUSD mock contract")
var harbingerCode = flag.String("harbingerCode", "contracts/harbinger_normalizer.tz", "filename of Harbinger contract")

var debug = flag.Bool("debug", false, "print debug information")
var verbose = flag.Bool("verbose", false, "print more information")

var rpcURL = flag.String("rpcurl", "http://localhost:20000", "url of a sandbox tezos-node rpc")
var alice = flag.String("alice", "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb", "address of the market creator, tezos-client needs to have the keys")
var bob = flag.String("bob", "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6", "address of a test user, tezos-client needs to have the keys")
var mockMode = flag.Bool("mock", false, "run in mock mode, this uses the tezos-client mock functionality rather than a sandbox to speed up tests")

var kUSD = ""
var optionsFactory = ""
var harbinger = ""
var marketFactory = ""
var marketDeployer = ""

// to make the tests run faster
// ✓ we can originate one kUSD mock and use it for all tests
// ✗ somehow set flextesa block timing to a fraction of second or on demand
// ✗ maybe this can be set in the baker binary if we run it manually

// the special function init runs before flags are available
// so we are using the TestMain hook to run init2 instead

func init2() {
	flag.Parse()
	d.Debug = *debug
	d.Verbose = *verbose

	d.Debugln("flag test:", *kUSDMockCode)

	// check for tezos-client
	if _, err := os.Stat("tezos-client"); os.IsNotExist(err) {
		fmt.Println("The tezos-client binary is required to run tests.")
		fmt.Println("If you don't have it, you can get it from the serokell repo.")
		fmt.Println(`$ wget https://github.com/serokell/tezos-packaging/releases/latest/download/tezos-client`)
		fmt.Println(`$ chmod +x tezos-client`)
		fmt.Println("If you already have the tezos-client binary, you can copy it into this folder.")
		return
	}

	// TODO check for running sandbox network, provide instructions if not
	// fmt.Println("Run the delphinet sandbox like this:")
	// fmt.Println("$ docker run --rm --name my-sandbox -e block_time=1 --detach -p 20000:20000 tqtezos/flextesa:20201214 delphibox start")
	// fmt.Println("If it gets sluggish or something breaks, just kill it:")
	// fmt.Println("$ docker kill my-sandbox")

	kUSDAddress, err := m.OriginateMockKUSD(*rpcURL, *alice, "mock", *kUSDMockCode)
	if err != nil {
		log.Fatalf("test setup: deploying kUSD: %v", err)
	}
	kUSD = kUSDAddress
	optionsFactoryAddress, err := m.OriginateOptionsFactory(*rpcURL, *alice, "optionsFactory", *optionsFactoryCode)
	if err != nil {
		log.Fatalf("test setup: deploying options factory: %v", err)
	}
	optionsFactory = optionsFactoryAddress

	var price int64 = 43272264136
	var harbingerTimestamp = time.Now().Add(time.Minute * 10).Unix()
	harbingerAddress, err := m.OriginateHarbinger(*rpcURL, *alice, "harbinger", *harbingerCode, price, harbingerTimestamp)
	if err != nil {
		log.Fatalf("test setup: deploying harbinger: %v", err)
	}
	harbinger = harbingerAddress

	marketFactoryAddress, err := m.OriginateMarketFactory(*rpcURL, *alice, "marketFactory", *marketFactoryCode, kUSD, harbinger, optionsFactory)
	if err != nil {
		log.Fatalf("test setup: deploying market factory: %v", err)
	}
	marketFactory = marketFactoryAddress
	marketDeployerAddress, err := m.OriginateMarketDeployer(*rpcURL, *alice, "marketDeployer", *marketDeployerCode, marketFactory, kUSD)
	if err != nil {
		log.Fatalf("test setup: deploying market deployer: %v", err)
	}
	marketDeployer = marketDeployerAddress
}

func TestMain(m *testing.M) {
	init2()
	code := m.Run()
	os.Exit(code)
}

// not sure if this is useful
func bigIntHelper(t *testing.T, nums []string) []*big.Int {
	bigNums := []*big.Int{}
	for _, n := range nums {
		bigNum, succ := big.NewInt(0).SetString(n, 10)
		if !succ {
			t.Fatalf("test setup: creating BigInt")
		}
		bigNums = append(bigNums, bigNum)
	}
	fmt.Println(bigNums)
	return bigNums
}

// a := bigIntHelper(t, []string{test.longBid, test.shortBid})
// longBid2 := a[0]
// shortBid2 := a[1]
// fmt.Println(longBid2)
// fmt.Println(shortBid2)

func TestInit(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	var tests = []struct {
		name             string
		longBid          string
		shortBid         string
		minCapital       string
		skewLimit        string // fraction, used as x/10^20
		biddingEndOffset int
		tradingEndOffset int
		want             error
	}{
		{"init 1 1", "1", "1", "2", "80000000000000000000", 5, 10, nil},
		{"init huge numbers", "123456000000000000000000000000000000000000000000000000000000", "123456000000000000000000000000000000000000000000000000000000", "246912000000000000000000000000000000000000000000000000000000", "80000000000000000000", 5, 10, nil},
		{"init with bidding end after trading end", "1", "1", "2", "80000000000000000000", 5, -10, fmt.Errorf("bidding end after trading end")},
		{"init skirting long skew", "8000000000000000000", "2000000000000000000", "10000000000000000000", "80000000000000000000", 5, 10, nil},
		{"init skirting short skew", "2000000000000000000", "8000000000000000000", "10000000000000000000", "80000000000000000000", 5, 10, nil},
		{"init with long skew", "8000000000000000001", "2000000000000000000", "10000000000000000001", "80000000000000000000", 5, 10, fmt.Errorf("long skew exceeded")},
		{"init with short skew", "2000000000000000000", "8000000000000000001", "10000000000000000001", "80000000000000000000", 5, 10, fmt.Errorf("short skew exceeded")},
		{"init with bids < min capital", "1", "1", "3", "80000000000000000000", 5, 10, fmt.Errorf("not enough capital provided")},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			longBid, succ := big.NewInt(0).SetString(test.longBid, 10)
			if !succ {
				t.Errorf("test setup: longBid: creating BigInt")
				t.FailNow()
			}
			shortBid, succ := big.NewInt(0).SetString(test.shortBid, 10)
			if !succ {
				t.Errorf("test setup: shortBid: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(test.minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(test.skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Minute * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Minute * time.Duration(test.tradingEndOffset)).Unix()
			market, _, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, longBid, shortBid, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, "BTC-USD", 36000000000, biddingEnd, tradingEnd)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			// check that initial bid values are good
			longBids, err := m.GetBidsMapID(*rpcURL, market, true)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			shortBids, err := m.GetBidsMapID(*rpcURL, market, false)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			long, err := m.GetBigMapElem(*rpcURL, *alice, longBids)
			if err != nil {
				t.Errorf("test setup: getting long bid: %v", err)
			}
			short, err := m.GetBigMapElem(*rpcURL, *alice, shortBids)
			if err != nil {
				t.Errorf("test setup: getting short bid: %v", err)
			}
			if longBid.Cmp(long) != 0 {
				t.Errorf("long bid: %d, wanted: %d", long, longBid)
			}
			if shortBid.Cmp(short) != 0 {
				t.Errorf("short bid: %d, wanted: %d", short, shortBid)
			}
			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
			}
		})
	}
}

func TestBidUser(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	var tests = []struct {
		name             string
		bid              string
		minCapital       string
		skewLimit        string // fraction, used as x/10^20
		side             bool
		biddingEndOffset int
		tradingEndOffset int
		want             error
	}{
		{"bid 1 long", "1", "26800000000000000000", "80000000000000000000", true, 5, 10, nil},
		{"bid 1 short", "1", "26800000000000000000", "80000000000000000000", false, 5, 10, nil},
		{"bid huge long", "123456000000000000000000000000000000000000000000000000000000", "26800000000000000000", "80000000000000000000", true, 5, 10, nil},
		{"bid huge short", "123456000000000000000000000000000000000000000000000000000000", "26800000000000000000", "80000000000000000000", false, 5, 10, nil},
		{"bid long after bidding end", "1", "26800000000000000000", "80000000000000000000", true, -5, 10, fmt.Errorf("bidding phase is over")},
		{"bid short after bidding end", "1", "26800000000000000000", "80000000000000000000", false, -5, 10, fmt.Errorf("bidding phase is over")},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			longBid, succ := big.NewInt(0).SetString("12300000000000000000", 10)
			if !succ {
				t.Errorf("test setup: longBid: creating BigInt")
				t.FailNow()
			}
			shortBid, succ := big.NewInt(0).SetString("14500000000000000000", 10)
			if !succ {
				t.Errorf("test setup: shortBid: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(test.minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(test.skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Minute * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Minute * time.Duration(test.tradingEndOffset)).Unix()
			// var price int64 = 43272264136
			// harbinger, err := m.OriginateHarbinger(*rpcURL, *alice, "harbinger", *harbingerCode, price, tradingEnd)
			// if err != nil {
			// 	t.Errorf("test setup: deploying harbinger: %v", err)
			// 	t.FailNow()
			// }
			market, _, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, longBid, shortBid, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, "BTC-USD", 36000000000, biddingEnd, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			testBid, succ := big.NewInt(0).SetString(test.bid, 10)
			if !succ {
				t.Errorf("test setup: testBid: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Bid(*rpcURL, *bob, market, testBid, test.side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: bidding: %v", err)
				t.FailNow()
			}
			// check that new bid value is good
			newBids, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid, err := m.GetBigMapElem(*rpcURL, *bob, newBids)
			if err != nil {
				t.Errorf("test setup: getting new bid: %v", err)
				t.FailNow()
			}
			if newBid.Cmp(testBid) != 0 {
				t.Errorf("newBid: %v, wanted: %v", newBid, test.bid)
			}
			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
			}
		})
	}
}

func TestBidCreator(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	var tests = []struct {
		name             string
		bid              string
		side             bool
		longBid          string
		shortBid         string
		minCapital       string
		skewLimit        string // fraction, used as x/10^20
		biddingEndOffset int
		tradingEndOffset int
		want             error
	}{
		{"bid skirting long skew", "1", true, "7999999999999999999", "2000000000000000000", "5000000000000000000", "80000000000000000000", 5, 10, nil},
		{"bid skirting short skew", "1", false, "2000000000000000000", "7999999999999999999", "5000000000000000000", "80000000000000000000", 5, 10, nil},
		{"bid with long skew", "1", true, "8000000000000000000", "2000000000000000000", "10000000000000000000", "80000000000000000000", 5, 10, fmt.Errorf("long skew exceeded")},
		{"bid with short skew", "1", false, "2000000000000000000", "8000000000000000000", "10000000000000000000", "80000000000000000000", 5, 10, fmt.Errorf("short skew exceeded")},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			longBid, succ := big.NewInt(0).SetString(test.longBid, 10)
			if !succ {
				t.Errorf("test setup: longBid: creating BigInt")
				t.FailNow()
			}
			shortBid, succ := big.NewInt(0).SetString(test.shortBid, 10)
			if !succ {
				t.Errorf("test setup: shortBid: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(test.minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(test.skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Minute * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Minute * time.Duration(test.tradingEndOffset)).Unix()
			// var price int64 = 43272264136
			// harbinger, err := m.OriginateHarbinger(*rpcURL, *alice, "harbinger", *harbingerCode, price, tradingEnd)
			// if err != nil {
			// 	t.Errorf("test setup: deploying harbinger: %v", err)
			// 	t.FailNow()
			// }
			market, _, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, longBid, shortBid, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, "BTC-USD", 36000000000, biddingEnd, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			testBid, succ := big.NewInt(0).SetString(test.bid, 10)
			if !succ {
				t.Errorf("test setup: testBid: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Bid(*rpcURL, *alice, market, testBid, test.side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: bidding: %v", err)
				t.FailNow()
			}
			// check that new bid value is good
			newBids, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid, err := m.GetBigMapElem(*rpcURL, *alice, newBids)
			if err != nil {
				t.Errorf("test setup: getting new bid: %v", err)
				t.FailNow()
			}
			var initialBid *big.Int
			if test.side {
				initialBid = longBid
			} else {
				initialBid = shortBid
			}
			want := big.NewInt(0).Add(initialBid, testBid)
			if newBid.Cmp(want) != 0 {
				t.Errorf("newBid: %v, wanted: %v", newBid, test.bid)
			}
			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
			}
		})
	}
}

func TestCancelUser(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	var tests = []struct {
		name             string
		bid              string
		cancel           string
		minCapital       string
		skewLimit        string // fraction, used as x/10^20
		side             bool
		biddingEndOffset int
		tradingEndOffset int
		want             error
	}{
		{"bid 2 and cancel 1 long", "2", "1", "26800000000000000000", "80000000000000000000", true, 5, 10, nil},
		{"bid 2 and cancel 1 short", "2", "1", "26800000000000000000", "80000000000000000000", false, 5, 10, nil},
		{"bid < cancel long", "2", "3", "26800000000000000000", "80000000000000000000", true, 5, 10, fmt.Errorf("cancel amount higher than bid")},
		{"bid < cancel short", "2", "3", "26800000000000000000", "80000000000000000000", false, 5, 10, fmt.Errorf("cancel amount higher than bid")},
		{"bid and cancel long with huge numbers", "123456000000000000000000000000000000000000000000000000000000", "113456000000000000000000000000000000000000000000000000000000", "26800000000000000000", "80000000000000000000", true, 5, 10, nil},
		{"bid and cancel short with huge numbers", "113456000000000000000000000000000000000000000000000000000000", "113456000000000000000000000000000000000000000000000000000000", "26800000000000000000", "80000000000000000000", false, 5, 10, nil},
		{"bid < cancel long with huge numbers", "123456000000000000000000000000000000000000000000000000000000", "133456000000000000000000000000000000000000000000000000000000", "26800000000000000000", "80000000000000000000", true, 5, 10, fmt.Errorf("cancel amount higher than bid")},
		{"bid < cancel short with huge numbers", "123456000000000000000000000000000000000000000000000000000000", "133456000000000000000000000000000000000000000000000000000000", "26800000000000000000", "80000000000000000000", false, 5, 10, fmt.Errorf("cancel amount higher than bid")},
		{"cancel long after bidding end", "2", "1", "26800000000000000000", "80000000000000000000", true, -5, 10, fmt.Errorf("bidding phase is over")},
		{"cancel short after bidding end", "2", "1", "26800000000000000000", "80000000000000000000", false, -5, 10, fmt.Errorf("bidding phase is over")},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			longBid, succ := big.NewInt(0).SetString("12300000000000000000", 10)
			if !succ {
				t.Errorf("test setup: longBid: creating BigInt")
				t.FailNow()
			}
			shortBid, succ := big.NewInt(0).SetString("14500000000000000000", 10)
			if !succ {
				t.Errorf("test setup: shortBid: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(test.minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(test.skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Minute * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Minute * time.Duration(test.tradingEndOffset)).Unix()
			// var price int64 = 43272264136
			// harbinger, err := m.OriginateHarbinger(*rpcURL, *alice, "harbinger", *harbingerCode, price, tradingEnd)
			// if err != nil {
			// 	t.Errorf("test setup: deploying harbinger: %v", err)
			// 	t.FailNow()
			// }
			market, _, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, longBid, shortBid, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, "BTC-USD", 36000000000, biddingEnd, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			testBid, succ := big.NewInt(0).SetString(test.bid, 10)
			if !succ {
				t.Errorf("test setup: testBid: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Bid(*rpcURL, *bob, market, testBid, test.side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: bidding: %v", err)
				t.FailNow()
			}
			// check that new bid value is good
			newBids, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid, err := m.GetBigMapElem(*rpcURL, *bob, newBids)
			if err != nil {
				t.Errorf("test setup: getting new bid: %v", err)
				t.FailNow()
			}
			if newBid.Cmp(testBid) != 0 {
				t.Errorf("test setup: got %v but bid %v", newBid, test.bid)
				// t.Fail()
			}
			testCancel, succ := big.NewInt(0).SetString(test.cancel, 10)
			if !succ {
				t.Errorf("test setup: testCancel: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Cancel(*rpcURL, *bob, market, testCancel, test.side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: cancelling bid: %v", err)
				t.FailNow()
			}
			// check that new bid value is good
			newBids2, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid2, err := m.GetBigMapElem(*rpcURL, *bob, newBids2)
			if err != nil {
				t.Errorf("test setup: getting new bid: %v", err)
				t.FailNow()
			}
			result := big.NewInt(0).Sub(testBid, testCancel)
			if newBid2.Cmp(result) != 0 {
				t.Errorf("newBid2: %v, wanted: %v", newBid2, result)
			}
			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
			}
		})
	}
}

func TestCancelOnly(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	var tests = []struct {
		name             string
		cancel           string
		minCapital       string
		skewLimit        string // fraction, used as x/10^20
		side             bool
		biddingEndOffset int
		tradingEndOffset int
		want             error
	}{
		{"no bid and cancel long", "1", "26800000000000000000", "80000000000000000000", true, 5, 10, fmt.Errorf("no bid to cancel")},
		{"no bid and cancel short", "1", "26800000000000000000", "80000000000000000000", false, 5, 10, fmt.Errorf("no bid to cancel")},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			longBid, succ := big.NewInt(0).SetString("12300000000000000000", 10)
			if !succ {
				t.Errorf("test setup: longBid: creating BigInt")
				t.FailNow()
			}
			shortBid, succ := big.NewInt(0).SetString("14500000000000000000", 10)
			if !succ {
				t.Errorf("test setup: shortBid: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(test.minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(test.skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Minute * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Minute * time.Duration(test.tradingEndOffset)).Unix()
			// var price int64 = 43272264136
			// harbinger, err := m.OriginateHarbinger(*rpcURL, *alice, "harbinger", *harbingerCode, price, tradingEnd)
			// if err != nil {
			// 	t.Errorf("test setup: deploying harbinger: %v", err)
			// 	t.FailNow()
			// }
			market, _, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, longBid, shortBid, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, "BTC-USD", 36000000000, biddingEnd, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			testCancel, succ := big.NewInt(0).SetString(test.cancel, 10)
			if !succ {
				t.Errorf("test setup: testCancel: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Cancel(*rpcURL, *bob, market, testCancel, test.side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: cancelling bid: %v", err)
				t.FailNow()
			}
			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
			}
		})
	}
}

func TestCancelCreator(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	var tests = []struct {
		name             string
		cancel           string
		side             bool
		longBid          string
		shortBid         string
		minCapital       string
		skewLimit        string // fraction, used as x/10^20
		biddingEndOffset int
		tradingEndOffset int
		want             error
	}{
		{"cancel reducing long skew", "1", true, "8000000000000000000", "2000000000000000000", "1", "80000000000000000000", 5, 10, nil},
		{"cancel reducing short skew", "1", false, "2000000000000000000", "8000000000000000000", "1", "80000000000000000000", 5, 10, nil},
		{"cancel skirting long skew", "1", false, "8000000000000000000", "2000000000000000001", "1", "80000000000000000000", 5, 10, nil},
		{"cancel skirting short skew", "1", true, "2000000000000000001", "8000000000000000000", "1", "80000000000000000000", 5, 10, nil},
		{"cancel with long skew", "1", false, "8000000000000000000", "2000000000000000000", "1", "80000000000000000000", 5, 10, fmt.Errorf("long skew exceeded")},
		{"cancel with short skew", "1", true, "2000000000000000000", "8000000000000000000", "1", "80000000000000000000", 5, 10, fmt.Errorf("short skew exceeded")},
		{"cancel with not enough capital", "1", true, "1", "1", "2", "80000000000000000000", 5, 10, fmt.Errorf("not enough capital left")},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			longBid, succ := big.NewInt(0).SetString(test.longBid, 10)
			if !succ {
				t.Errorf("test setup: longBid: creating BigInt")
				t.FailNow()
			}
			shortBid, succ := big.NewInt(0).SetString(test.shortBid, 10)
			if !succ {
				t.Errorf("test setup: shortBid: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(test.minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(test.skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Minute * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Minute * time.Duration(test.tradingEndOffset)).Unix()
			// var price int64 = 43272264136
			// harbinger, err := m.OriginateHarbinger(*rpcURL, *alice, "harbinger", *harbingerCode, price, tradingEnd)
			// if err != nil {
			// 	t.Errorf("test setup: deploying harbinger: %v", err)
			// 	t.FailNow()
			// }
			market, _, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, longBid, shortBid, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, "BTC-USD", 36000000000, biddingEnd, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			testCancel, succ := big.NewInt(0).SetString(test.cancel, 10)
			if !succ {
				t.Errorf("test setup: testCancel: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Cancel(*rpcURL, *alice, market, testCancel, test.side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: cancelling bid: %v", err)
				t.FailNow()
			}
			// check that new bid value is good
			newBids, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid, err := m.GetBigMapElem(*rpcURL, *alice, newBids)
			if err != nil {
				t.Errorf("test setup: getting new bid: %v", err)
				t.FailNow()
			}
			var initialBid *big.Int
			if test.side {
				initialBid = longBid
			} else {
				initialBid = shortBid
			}
			want := big.NewInt(0).Sub(initialBid, testCancel)
			if newBid.Cmp(want) != 0 {
				t.Errorf("newBid: %v, wanted: %v", newBid, test.cancel)
			}
			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
			}
		})
	}
}

func TestClaimUser(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	initialLong := "12300000000000000000"
	initialShort := "14500000000000000000"
	minCapital := "26800000000000000000"
	skewLimit := "80000000000000000000" // fraction, used as x/10^20
	var tests = []struct {
		name             string
		bid              string
		side             bool
		longTokens       string
		shortTokens      string
		biddingEndOffset int
		tradingEndOffset int
		want             error
	}{
		{"claim 22 long", "2200000000000000000", true, "4400000000000000000", "0", 9, 30, nil},
		{"claim 22 short", "2200000000000000000", false, "0", "3820359281437125744", 9, 30, nil},
		{"claim tiny long", "1", true, "2", "0", 9, 30, nil},
		{"claim tiny short", "1", false, "0", "1", 9, 30, nil},
		{"claim huge long", "123456000000000000000000000000000000000000000000000000000000", true, "123456000000000000123456000000000000123456000000000000123456", "0", 9, 30, nil},
		{"claim huge short", "123456000000000000000000000000000000000000000000000000000000", false, "0", "123456000000000000123456000000000000123456000000000000123456", 9, 30, nil},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			initialLong, succ := big.NewInt(0).SetString(initialLong, 10)
			if !succ {
				t.Errorf("test setup: initialLong: creating BigInt")
				t.FailNow()
			}
			initialShort, succ := big.NewInt(0).SetString(initialShort, 10)
			if !succ {
				t.Errorf("test setup: initialShort: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Second * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Second * time.Duration(test.tradingEndOffset)).Unix()
			biddingTicker := time.NewTimer(time.Second * (1 + time.Duration(test.biddingEndOffset)))
			// var price int64 = 43272264136
			// harbinger, err := m.OriginateHarbinger(*rpcURL, *alice, "harbinger", *harbingerCode, price, tradingEnd)
			// if err != nil {
			// 	t.Errorf("test setup: deploying harbinger: %v", err)
			// 	t.FailNow()
			// }
			market, fa2, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, initialLong, initialShort, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, "BTC-USD", 36000000000, biddingEnd, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			testBid, succ := big.NewInt(0).SetString(test.bid, 10)
			if !succ {
				t.Errorf("test setup: testBid: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Bid(*rpcURL, *bob, market, testBid, test.side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: bidding: %v", err)
				t.FailNow()
			}

			// check that new bid value is good
			newBids, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid, err := m.GetBigMapElem(*rpcURL, *bob, newBids)
			if err != nil {
				t.Errorf("test setup: getting new bid: %v", err)
				t.FailNow()
			}
			if newBid.Cmp(testBid) != 0 {
				t.Errorf("test setup: got %v but bid %v", newBid, test.bid)
			}

			// wait for bidding to end
			<-biddingTicker.C

			// kUSD mock doesn't require approves for now
			err = m.Claim(*rpcURL, *alice, market, *bob)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: claiming: %v", err)
				t.FailNow()
			}

			// wait for claim to make it into a block
			time.Sleep(time.Second * 1)

			// check that new bid value is good
			newBids2, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid2, err := m.GetBigMapElem(*rpcURL, *bob, newBids2)
			if err != nil {
				// bid has been claimed so we expect to find no value
				if !strings.Contains(err.Error(), fmt.Sprintf("no value for %s", *bob)) {
					t.Errorf("test setup: getting new bid: %v", err)
				}
			}
			result := big.NewInt(0)
			if newBid2.Cmp(result) != 0 {
				t.Errorf("newBid2: %v, wanted: %v", newBid2, result)
			}

			// get options bigmap, get elem for bob, check value
			optionMap, err := m.GetOptionsMapID(*rpcURL, fa2)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			longOptions, shortOptions, err := m.GetOptionsElem(*rpcURL, *bob, optionMap)
			if err != nil {
				t.Errorf("test setup: getting option tokens: %v", err)
				t.FailNow()
			}

			var longBid *big.Int
			var shortBid *big.Int
			if test.side {
				longBid = testBid
				shortBid = big.NewInt(0)
			} else {
				longBid = big.NewInt(0)
				shortBid = testBid
			}

			// longPrice = totalLong * 10^18 / total
			// userTokens = userBid * 10^18 / longPrice

			tenTo18 := big.NewInt(0).Exp(big.NewInt(10), big.NewInt(18), nil)
			totalLong := big.NewInt(0).Add(initialLong, longBid)
			totalShort := big.NewInt(0).Add(initialShort, shortBid)
			total := big.NewInt(0).Add(totalLong, totalShort)
			longPrice := big.NewInt(0).Div(big.NewInt(0).Mul(totalLong, tenTo18), total)
			if longPrice.Cmp(big.NewInt(0)) == 0 {
				longPrice = big.NewInt(1)
			}
			longTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(longBid, tenTo18), longPrice)
			// short price = 1 - long price (but Michelson only has integers)
			// shortPrice = 10^18 - longPrice
			shortPrice := big.NewInt(0).Sub(tenTo18, longPrice)
			shortTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(shortBid, tenTo18), shortPrice)

			longTokens, succ := big.NewInt(0).SetString(test.longTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}
			shortTokens, succ := big.NewInt(0).SetString(test.shortTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}

			// TODO choose one of these ways to check
			// redoing the math requires rewriting when we change the contract math
			// the other one requires doing the math ahead of time and hardcoding results

			// check claimed options by redoing the math
			if longTokens2.Cmp(longTokens) != 0 {
				t.Errorf("longTokens2: %v, longTokens: %v", longTokens2, longTokens)
			}
			if shortTokens2.Cmp(shortTokens) != 0 {
				t.Errorf("shortTokens2: %v, shortTokens: %v", shortTokens2, shortTokens)
			}

			// check claimed options against our expected values
			if longOptions.Cmp(longTokens) != 0 {
				t.Errorf("longOptions: %v, wanted: %v", longOptions, longTokens)
			}
			if shortOptions.Cmp(shortTokens) != 0 {
				t.Errorf("shortOptions: %v, wanted: %v", shortOptions, shortTokens)
			}

			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
			}
		})
	}
}

func TestClaimAfterCancel(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	initialLong := "12300000000000000000"
	initialShort := "14500000000000000000"
	minCapital := "26800000000000000000"
	skewLimit := "80000000000000000000" // fraction, used as x/10^20
	var tests = []struct {
		name             string
		bid              string
		cancel           string
		side             bool
		longTokens       string
		shortTokens      string
		biddingEndOffset int
		tradingEndOffset int
		want             error
	}{
		{"claim 22 long cancel 12.3", "2200000000000000000", "1230000000000000000", true, "2030449027882441600", "0", 11, 19, nil},
		{"claim 22 short cancel 12.3", "2200000000000000000", "1230000000000000000", false, "0", "1740866903829976391", 11, 19, nil},
		{"claim tiny long", "1", "1", true, "0", "0", 11, 19, nil},
		{"claim tiny short", "1", "1", false, "0", "0", 11, 19, nil},
		{"claim huge long", "123456000000000000000000000000000000000000000000000000000000", "23456000000000000000000000000000000000000000000000000000000", true, "100140736000000000027673964494929920007647720013428319743655", "0", 11, 19, nil},
		{"claim huge short", "123456000000000000000000000000000000000000000000000000000000", "23456000000000000000000000000000000000000000000000000000000", false, "0", "100000000000000000100000000000000000100000000000000000100000", 11, 19, nil},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			initialLong, succ := big.NewInt(0).SetString(initialLong, 10)
			if !succ {
				t.Errorf("test setup: initialLong: creating BigInt")
				t.FailNow()
			}
			initialShort, succ := big.NewInt(0).SetString(initialShort, 10)
			if !succ {
				t.Errorf("test setup: initialShort: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Second * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Second * time.Duration(test.tradingEndOffset)).Unix()
			biddingTicker := time.NewTimer(time.Second * (1 + time.Duration(test.biddingEndOffset)))
			market, fa2, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, initialLong, initialShort, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, "BTC-USD", 36000000000, biddingEnd, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			testBid, succ := big.NewInt(0).SetString(test.bid, 10)
			if !succ {
				t.Errorf("test setup: testBid: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Bid(*rpcURL, *bob, market, testBid, test.side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: bidding: %v", err)
				t.FailNow()
			}

			// check that new bid value is good
			newBids, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid, err := m.GetBigMapElem(*rpcURL, *bob, newBids)
			if err != nil {
				t.Errorf("test setup: getting new bid: %v", err)
				t.FailNow()
			}
			if newBid.Cmp(testBid) != 0 {
				t.Errorf("test setup: got %v but bid %v", newBid, test.bid)
			}

			testCancel, succ := big.NewInt(0).SetString(test.cancel, 10)
			if !succ {
				t.Errorf("test setup: testCancel: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Cancel(*rpcURL, *bob, market, testCancel, test.side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: cancelling: %v", err)
				t.FailNow()
			}

			// check that new bid value after cancel is good
			bidAfterCancel, err := m.GetBigMapElem(*rpcURL, *bob, newBids)
			if err != nil {
				t.Errorf("test setup: getting bid after cancel: %v", err)
				t.FailNow()
			}
			wantBid := big.NewInt(0).Sub(testBid, testCancel)
			if bidAfterCancel.Cmp(wantBid) != 0 {
				t.Errorf("test setup: got %v but bid %v", bidAfterCancel, wantBid)
			}

			// wait for bidding to end
			<-biddingTicker.C

			// kUSD mock doesn't require approves for now
			err = m.Claim(*rpcURL, *alice, market, *bob)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: claiming: %v", err)
				t.FailNow()
			}

			// wait for claim to make it into a block
			time.Sleep(time.Second * 1)

			// check that new bid value is good
			newBids2, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid2, err := m.GetBigMapElem(*rpcURL, *bob, newBids2)
			if err != nil {
				// bid has been claimed so we expect to find no value
				if !strings.Contains(err.Error(), fmt.Sprintf("no value for %s", *bob)) {
					t.Errorf("test setup: getting new bid: %v", err)
				}
			}
			result := big.NewInt(0)
			if newBid2.Cmp(result) != 0 {
				t.Errorf("newBid2: %v, wanted: %v", newBid2, result)
			}

			// get options bigmap, get elem for bob, check value
			optionMap, err := m.GetOptionsMapID(*rpcURL, fa2)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			longOptions, shortOptions, err := m.GetOptionsElem(*rpcURL, *bob, optionMap)
			if err != nil {
				t.Errorf("test setup: getting option tokens: %v", err)
				t.FailNow()
			}

			var longBid *big.Int
			var shortBid *big.Int
			if test.side {
				longBid = big.NewInt(0).Sub(testBid, testCancel)
				shortBid = big.NewInt(0)
			} else {
				longBid = big.NewInt(0)
				shortBid = big.NewInt(0).Sub(testBid, testCancel)
			}

			tenTo18 := big.NewInt(0).Exp(big.NewInt(10), big.NewInt(18), nil)

			marketEarnings, err := m.GetMarketEarnings(*rpcURL, market)
			if err != nil {
				t.Errorf("test setup: getting market earnings: %v", err)
			}
			// check that marketProfit value is correct
			mp, err := m.GetCancelFeeMultiplier(*rpcURL, market)
			if err != nil {
				t.Errorf("test setup: getting cancel fee multiplier: %v", err)
			}
			marketEarnings2 := big.NewInt(0).Div(big.NewInt(0).Mul(testCancel, mp), tenTo18)
			if marketEarnings.Cmp(marketEarnings2) != 0 {
				t.Errorf("marketEarnings: %v, wanted: %v", marketEarnings, marketEarnings2)
			}

			// check that market profit improves prices correctly
			// market profit (from cancel fees) is added to total
			// total = totalLong + totalShort + marketProfit
			// longPrice = totalLong * 10^18 / total
			// userTokens = userBid * 10^18 / longPrice

			totalLong := big.NewInt(0).Add(initialLong, longBid)
			totalShort := big.NewInt(0).Add(initialShort, shortBid)
			total := big.NewInt(0).Add(big.NewInt(0).Add(totalLong, totalShort), marketEarnings)
			longPrice := big.NewInt(0).Div(big.NewInt(0).Mul(totalLong, tenTo18), total)
			if longPrice.Cmp(big.NewInt(0)) == 0 {
				longPrice = big.NewInt(1)
			}
			longTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(longBid, tenTo18), longPrice)
			// short price = 1 - long price (but Michelson only has integers)
			// shortPrice = 10^18 - longPrice
			shortPrice := big.NewInt(0).Sub(tenTo18, longPrice)
			shortTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(shortBid, tenTo18), shortPrice)

			longTokens, succ := big.NewInt(0).SetString(test.longTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}
			shortTokens, succ := big.NewInt(0).SetString(test.shortTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}

			// TODO choose one of these ways to check
			// redoing the math requires rewriting when we change the contract math
			// the other one requires doing the math ahead of time and hardcoding results

			// check claimed options by redoing the math
			if longTokens2.Cmp(longTokens) != 0 {
				t.Errorf("longTokens2: %v, longTokens: %v", longTokens2, longTokens)
			}
			if shortTokens2.Cmp(shortTokens) != 0 {
				t.Errorf("shortTokens2: %v, shortTokens: %v", shortTokens2, shortTokens)
			}

			// check claimed options against our expected values
			if longOptions.Cmp(longTokens) != 0 {
				t.Errorf("longOptions: %v, wanted: %v", longOptions, longTokens)
			}
			if shortOptions.Cmp(shortTokens) != 0 {
				t.Errorf("shortOptions: %v, wanted: %v", shortOptions, shortTokens)
			}

			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
			}
		})
	}
}

func TestClaimTiming(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	initialLong := "12300000000000000000"
	initialShort := "14500000000000000000"
	minCapital := "26800000000000000000"
	skewLimit := "80000000000000000000" // fraction, used as x/10^20
	var tests = []struct {
		name             string
		bid              string
		side             bool
		longTokens       string
		shortTokens      string
		biddingEndOffset int
		tradingEndOffset int
		wait             bool // wait for bidding to end?
		want             error
	}{
		{"claim before bidding end long", "1", true, "2", "0", 15, 30, false, fmt.Errorf("still in bidding phase")},
		{"claim before bidding end short", "1", false, "0", "1", 15, 30, false, fmt.Errorf("still in bidding phase")},
		{"claim after trading end long", "1", true, "2", "0", 9, 10, true, nil},   // claiming after trading end is allowed
		{"claim after trading end short", "1", false, "0", "1", 9, 10, true, nil}, // claiming after trading end is allowed
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			initialLong, succ := big.NewInt(0).SetString(initialLong, 10)
			if !succ {
				t.Errorf("test setup: initialLong: creating BigInt")
				t.FailNow()
			}
			initialShort, succ := big.NewInt(0).SetString(initialShort, 10)
			if !succ {
				t.Errorf("test setup: initialShort: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Second * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Second * time.Duration(test.tradingEndOffset)).Unix()
			biddingTicker := time.NewTimer(time.Second * (1 + time.Duration(test.biddingEndOffset)))

			market, fa2, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, initialLong, initialShort, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, "BTC-USD", 36000000000, biddingEnd, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			testBid, succ := big.NewInt(0).SetString(test.bid, 10)
			if !succ {
				t.Errorf("test setup: testBid: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Bid(*rpcURL, *bob, market, testBid, test.side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: bidding: %v", err)
				t.FailNow()
			}

			// check that new bid value is good
			newBids, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid, err := m.GetBigMapElem(*rpcURL, *bob, newBids)
			if err != nil {
				t.Errorf("test setup: getting new bid: %v", err)
				t.FailNow()
			}
			if newBid.Cmp(testBid) != 0 {
				t.Errorf("test setup: got %v but bid %v", newBid, test.bid)
			}

			// wait for bidding to end
			if test.wait {
				<-biddingTicker.C
			}

			// kUSD mock doesn't require approves for now
			err = m.Claim(*rpcURL, *alice, market, *bob)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: claiming: %v", err)
				t.FailNow()
			}

			// wait for claim to make it into a block
			time.Sleep(time.Second * 1)

			// check that new bid value is good
			newBids2, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid2, err := m.GetBigMapElem(*rpcURL, *bob, newBids2)
			if err != nil {
				// bid has been claimed so we expect to find no value
				if !strings.Contains(err.Error(), fmt.Sprintf("no value for %s", *bob)) {
					t.Errorf("test setup: getting new bid: %v", err)
				}
			}
			result := big.NewInt(0)
			if newBid2.Cmp(result) != 0 {
				t.Errorf("newBid2: %v, wanted: %v", newBid2, result)
			}

			// get options bigmap, get elem for bob, check value
			optionMap, err := m.GetOptionsMapID(*rpcURL, fa2)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			longOptions, shortOptions, err := m.GetOptionsElem(*rpcURL, *bob, optionMap)
			if err != nil {
				t.Errorf("test setup: getting option tokens: %v", err)
				t.FailNow()
			}

			var longBid *big.Int
			var shortBid *big.Int
			if test.side {
				longBid = testBid
				shortBid = big.NewInt(0)
			} else {
				longBid = big.NewInt(0)
				shortBid = testBid
			}

			// longPrice = totalLong * 10^18 / total
			// userTokens = userBid * 10^18 / longPrice

			tenTo18 := big.NewInt(0).Exp(big.NewInt(10), big.NewInt(18), nil)
			totalLong := big.NewInt(0).Add(initialLong, longBid)
			totalShort := big.NewInt(0).Add(initialShort, shortBid)
			total := big.NewInt(0).Add(totalLong, totalShort)
			longPrice := big.NewInt(0).Div(big.NewInt(0).Mul(totalLong, tenTo18), total)
			longTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(longBid, tenTo18), longPrice)
			// short price = 1 - long price (but Michelson only has integers)
			// shortPrice = 10^18 - longPrice
			shortPrice := big.NewInt(0).Sub(tenTo18, longPrice)
			shortTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(shortBid, tenTo18), shortPrice)

			longTokens, succ := big.NewInt(0).SetString(test.longTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}
			shortTokens, succ := big.NewInt(0).SetString(test.shortTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}

			// TODO choose one of these ways to check
			// redoing the math requires rewriting when we change the contract math
			// the other one requires doing the math ahead of time and hardcoding results

			// check claimed options by redoing the math
			if longTokens2.Cmp(longTokens) != 0 {
				t.Errorf("longTokens2: %v, longTokens: %v", longTokens2, longTokens)
			}
			if shortTokens2.Cmp(shortTokens) != 0 {
				t.Errorf("shortTokens2: %v, shortTokens: %v", shortTokens2, shortTokens)
			}

			// check claimed options against our expected values
			if longOptions.Cmp(longTokens) != 0 {
				t.Errorf("longOptions: %v, wanted: %v", longOptions, longTokens)
			}
			if shortOptions.Cmp(shortTokens) != 0 {
				t.Errorf("shortOptions: %v, wanted: %v", shortOptions, shortTokens)
			}

			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
			}
		})
	}
}

func TestGetPrices(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	longBid := "1"
	shortBid := "1"
	minCapital := "2"
	skewLimit := "80000000000000000000" // fraction, used as x/10^20
	var harbingerPrice int64 = 43272264136
	var tests = []struct {
		name                string
		asset               string
		targetPrice         int64
		winningSide         bool
		biddingEndOffset    int
		tradingEndOffset    int
		harbingerTimeOffset int
		want                error
	}{
		{"price above", "BTC-USD", 36000000000, true, 5, 10, 10, nil},
		{"price below", "BTC-USD", 46000000000, false, 5, 10, 10, nil},
		{"price equal (longs)", "BTC-USD", 43272264136, true, 5, 10, 10, nil},
		{"too soon bidding", "BTC-USD", 36000000000, true, 15, 20, 20, fmt.Errorf("still in bidding or trading phase")},
		{"too soon trading", "BTC-USD", 36000000000, true, 5, 17, 17, fmt.Errorf("still in bidding or trading phase")},
		{"future price", "BTC-USD", 36000000000, true, 5, 9, 100, fmt.Errorf("price is from the future")},
		{"outdated price", "BTC-USD", 36000000000, true, 5, 9, 0, fmt.Errorf("price is outdated")},
		{"unknown asset", "NYT-SHIT", 36000000000, true, 5, 9, 9, fmt.Errorf("with Unit")}, // Harbinger is deployed without error messages
		// Harbinger sends price for another asset: fails with "price is for another asset" & Harbinger code would need to be able to do this
		// incoming price from non-Harbinger caller: fails with "only Harbinger can call this entrypoint"
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			longBid, succ := big.NewInt(0).SetString(longBid, 10)
			if !succ {
				t.Errorf("test setup: longBid: creating BigInt")
				t.FailNow()
			}
			shortBid, succ := big.NewInt(0).SetString(shortBid, 10)
			if !succ {
				t.Errorf("test setup: shortBid: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Second * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Second * time.Duration(test.tradingEndOffset)).Unix()
			tradingTicker := time.NewTimer(time.Second * (1 + time.Duration(test.tradingEndOffset)))

			// use a specific configuration for Harbinger for each test
			// using a separate Harbiner means we need a separate deployer as well
			harbingerTime := time.Now().Add(time.Second * time.Duration(test.harbingerTimeOffset)).Unix()
			harbinger, err := m.OriginateHarbinger(*rpcURL, *alice, "harbinger", *harbingerCode, harbingerPrice, harbingerTime)
			if err != nil {
				t.Errorf("test setup: deploying harbinger: %v", err)
				t.FailNow()
			}
			marketFactory, err := m.OriginateMarketFactory(*rpcURL, *alice, "marketFactory", *marketFactoryCode, kUSD, harbinger, optionsFactory)
			if err != nil {
				t.Errorf("test setup: deploying market factory: %v", err)
				t.FailNow()
			}
			marketDeployer, err := m.OriginateMarketDeployer(*rpcURL, *alice, "marketDeployer", *marketDeployerCode, marketFactory, kUSD)
			if err != nil {
				t.Errorf("test setup: deploying market deployer: %v", err)
				t.FailNow()
			}
			market, _, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, longBid, shortBid, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, test.asset, test.targetPrice, biddingEnd, tradingEnd)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			// check that initial bid values are good
			longBids, err := m.GetBidsMapID(*rpcURL, market, true)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			shortBids, err := m.GetBidsMapID(*rpcURL, market, false)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			long, err := m.GetBigMapElem(*rpcURL, *alice, longBids)
			if err != nil {
				t.Errorf("test setup: getting long bid: %v", err)
			}
			short, err := m.GetBigMapElem(*rpcURL, *alice, shortBids)
			if err != nil {
				t.Errorf("test setup: getting short bid: %v", err)
			}
			if longBid.Cmp(long) != 0 {
				t.Errorf("long bid: %d, wanted: %d", long, longBid)
			}
			if shortBid.Cmp(short) != 0 {
				t.Errorf("short bid: %d, wanted: %d", short, shortBid)
			}

			if test.want == nil || !strings.Contains(test.want.Error(), "phase") {
				// wait for trading end and get prices from Harbinger
				<-tradingTicker.C
			}

			// call %get in harbinger, specify asset string and %receive_prices of market
			err = m.GetPrices(*rpcURL, *alice, market, test.asset, harbinger)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: getting prices: %v", err)
				t.FailNow()
			}

			winningSide, err := m.GetWinningSide(*rpcURL, market)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: getting winning side: %v", err)
				t.FailNow()
			}

			if winningSide != test.winningSide {
				t.Errorf("winningSide: %t, wanted: %t", winningSide, test.winningSide)
			}

			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
			}
		})
	}
}

func TestExerciseUser(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	initialLong := "12300000000000000000"
	initialShort := "14500000000000000000"
	minCapital := "26800000000000000000"
	skewLimit := "80000000000000000000" // fraction, used as x/10^20
	var tests = []struct {
		name             string
		bid              string
		side             bool
		longTokens       string
		shortTokens      string
		harbingerPrice   int64
		wantKUSD         string
		biddingEndOffset int
		tradingEndOffset int
		want             error
	}{
		{"exercise 22 long won", "2200000000000000000", true, "4400000000000000000", "0", 43272264136, "4386800000000000000", 20, 21, nil},
		{"exercise 22 long lost", "2200000000000000000", true, "4400000000000000000", "0", 23272264136, "0", 20, 21, nil},
		{"exercise 22 short lost", "2200000000000000000", false, "0", "3820359281437125744", 43272264136, "0", 20, 21, nil},
		{"exercise 22 short won", "2200000000000000000", false, "0", "3820359281437125744", 23272264136, "3808898203592814367", 20, 21, nil},
		{"exercise tiny long won", "1", true, "2", "0", 43272264136, "2", 20, 21, nil},
		{"exercise tiny long lost", "1", true, "2", "0", 23272264136, "0", 20, 21, nil},
		{"exercise tiny short lost", "1", false, "0", "1", 43272264136, "0", 20, 21, nil},
		{"exercise tiny short won", "1", false, "0", "1", 23272264136, "1", 20, 21, nil},
		{"exercise huge long won", "123456000000000000000000000000000000000000000000000000000000", true, "123456000000000000123456000000000000123456000000000000123456", "0", 43272264136, "123085632000000000123085632000000000123085632000000000123086", 20, 21, nil},
		{"exercise huge long lost", "123456000000000000000000000000000000000000000000000000000000", true, "123456000000000000123456000000000000123456000000000000123456", "0", 23272264136, "0", 20, 21, nil},
		{"exercise huge short lost", "123456000000000000000000000000000000000000000000000000000000", false, "0", "123456000000000000123456000000000000123456000000000000123456", 43272264136, "0", 20, 21, nil},
		{"exercise huge short won", "123456000000000000000000000000000000000000000000000000000000", false, "0", "123456000000000000123456000000000000123456000000000000123456", 23272264136, "123085632000000000123085632000000000123085632000000000123086", 20, 21, nil},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			initialLong, succ := big.NewInt(0).SetString(initialLong, 10)
			if !succ {
				t.Errorf("test setup: initialLong: creating BigInt")
				t.FailNow()
			}
			initialShort, succ := big.NewInt(0).SetString(initialShort, 10)
			if !succ {
				t.Errorf("test setup: initialShort: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Second * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Second * time.Duration(test.tradingEndOffset)).Unix()
			biddingTicker := time.NewTimer(time.Second * (1 + time.Duration(test.biddingEndOffset)))
			tradingTicker := time.NewTimer(time.Second * (1 + time.Duration(test.tradingEndOffset)))

			harbinger, err := m.OriginateHarbinger(*rpcURL, *alice, "harbinger", *harbingerCode, test.harbingerPrice, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying harbinger: %v", err)
				t.FailNow()
			}
			marketFactory, err := m.OriginateMarketFactory(*rpcURL, *alice, "marketFactory", *marketFactoryCode, kUSD, harbinger, optionsFactory)
			if err != nil {
				t.Errorf("test setup: deploying market factory: %v", err)
				t.FailNow()
			}
			marketDeployer, err := m.OriginateMarketDeployer(*rpcURL, *alice, "marketDeployer", *marketDeployerCode, marketFactory, kUSD)
			if err != nil {
				t.Errorf("test setup: deploying market deployer: %v", err)
				t.FailNow()
			}

			market, options, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, initialLong, initialShort, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, "BTC-USD", 36000000000, biddingEnd, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			testBid, succ := big.NewInt(0).SetString(test.bid, 10)
			if !succ {
				t.Errorf("test setup: testBid: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Bid(*rpcURL, *bob, market, testBid, test.side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: bidding: %v", err)
				t.FailNow()
			}

			// check that new bid value is good
			newBids, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid, err := m.GetBigMapElem(*rpcURL, *bob, newBids)
			if err != nil {
				t.Errorf("test setup: getting new bid: %v", err)
				t.FailNow()
			}
			if newBid.Cmp(testBid) != 0 {
				t.Errorf("test setup: got %v but bid %v", newBid, test.bid)
			}

			// wait for bidding to end
			<-biddingTicker.C

			// kUSD mock doesn't require approves for now
			err = m.Claim(*rpcURL, *alice, market, *bob)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: claiming: %v", err)
				t.FailNow()
			}

			// wait for claim to make it into a block
			time.Sleep(time.Second * 1)

			// check that new bid value is good
			newBids2, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid2, err := m.GetBigMapElem(*rpcURL, *bob, newBids2)
			if err != nil {
				// bid has been claimed so we expect to find no value
				if !strings.Contains(err.Error(), fmt.Sprintf("no value for %s", *bob)) {
					t.Errorf("test setup: getting new bid: %v", err)
				}
			}
			result := big.NewInt(0)
			if newBid2.Cmp(result) != 0 {
				t.Errorf("newBid2: %v, wanted: %v", newBid2, result)
			}

			// get options bigmap, get elem for bob, check value
			optionMap, err := m.GetOptionsMapID(*rpcURL, options)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			longOptions, shortOptions, err := m.GetOptionsElem(*rpcURL, *bob, optionMap)
			if err != nil {
				t.Errorf("test setup: getting option tokens: %v", err)
				t.FailNow()
			}

			var longBid *big.Int
			var shortBid *big.Int
			if test.side {
				longBid = testBid
				shortBid = big.NewInt(0)
			} else {
				longBid = big.NewInt(0)
				shortBid = testBid
			}

			// longPrice = totalLong * 10^18 / total
			// longPrice set to 1 if 0
			// userTokens = userBid * 10^18 / longPrice

			tenTo18 := big.NewInt(0).Exp(big.NewInt(10), big.NewInt(18), nil)
			totalLong := big.NewInt(0).Add(initialLong, longBid)
			totalShort := big.NewInt(0).Add(initialShort, shortBid)
			total := big.NewInt(0).Add(totalLong, totalShort)
			longPrice := big.NewInt(0).Div(big.NewInt(0).Mul(totalLong, tenTo18), total)
			if longPrice.Cmp(big.NewInt(0)) == 0 {
				longPrice = big.NewInt(1)
			}
			longTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(longBid, tenTo18), longPrice)
			// short price = 1 - long price (but Michelson only has integers)
			// shortPrice = 10^18 - longPrice
			shortPrice := big.NewInt(0).Sub(tenTo18, longPrice)
			shortTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(shortBid, tenTo18), shortPrice)

			longTokens, succ := big.NewInt(0).SetString(test.longTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}
			shortTokens, succ := big.NewInt(0).SetString(test.shortTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}

			// TODO choose one of these ways to check
			// redoing the math requires rewriting when we change the contract math
			// the other one requires doing the math ahead of time and hardcoding results

			// check claimed options by redoing the math
			if longTokens2.Cmp(longTokens) != 0 {
				t.Errorf("longTokens2: %v, longTokens: %v", longTokens2, longTokens)
			}
			if shortTokens2.Cmp(shortTokens) != 0 {
				t.Errorf("shortTokens2: %v, shortTokens: %v", shortTokens2, shortTokens)
			}

			// check claimed options against our expected values
			if longOptions.Cmp(longTokens) != 0 {
				t.Errorf("longOptions: %v, wanted: %v", longOptions, longTokens)
			}
			if shortOptions.Cmp(shortTokens) != 0 {
				t.Errorf("shortOptions: %v, wanted: %v", shortOptions, shortTokens)
			}

			// wait for trading end and exercise
			<-tradingTicker.C

			// call %get in harbinger, specify asset string and %receive_prices of market
			err = m.GetPrices(*rpcURL, *alice, market, "BTC-USD", harbinger)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: getting prices: %v", err)
				t.FailNow()
			}

			winningSide, err := m.GetWinningSide(*rpcURL, market)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: getting winning side: %v", err)
				t.FailNow()
			}

			// exercise by calling get_balance in options, specify %exercise in market
			// we can specify any number of users and which tokens we are interested in (long or short)
			// however the losing tokens are simply ignored so there is no point
			// TODO tests where we call the other side (losing side tokens should be ignored, nothing redemeed)
			paidKUSD, err := m.Exercise(*rpcURL, *alice, market, options, kUSD, []string{*bob}, winningSide)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: exercising: %v", err)
				t.FailNow()
			}

			// math to calculate expected paidKUSD
			// tokens             (exercise_fee_multiplier     tokens    ten to 18)
			// 4400000000000000001-3000000000000000*4400000000000000001/10^18

			// check redeemed kUSD is correct
			wantKUSD, succ := big.NewInt(0).SetString(test.wantKUSD, 10)
			if !succ {
				t.Errorf("test setup: wantKUSD: creating BigInt")
				t.FailNow()
			}
			if paidKUSD.Cmp(wantKUSD) != 0 {
				t.Errorf("paidKUSD: %v, wantKUSD: %v", paidKUSD, wantKUSD)
				t.FailNow()
			}

			// get options for user, winning options should have been burned, losing are untouched
			longOptions2, shortOptions2, err := m.GetOptionsElem(*rpcURL, *bob, optionMap)
			if err != nil {
				t.Errorf("test setup: getting option tokens: %v", err)
				t.FailNow()
			}
			if winningSide {
				longOptions = big.NewInt(0)
			} else {
				shortOptions = big.NewInt(0)
			}
			if longOptions2.Cmp(longOptions) != 0 {
				t.Errorf("longOptions2: %v, longOptions: %v", longOptions2, longOptions)
				t.FailNow()
			}
			if shortOptions2.Cmp(shortOptions) != 0 {
				t.Errorf("shortOptions2: %v, shortOptions: %v", shortOptions2, shortOptions)
				t.FailNow()
			}

			// TODO check creator profit
			// math to calculate expected creator profit
			// exercise_fee_multiplier     tokens    ten to 18
			// 3000000000000000*4400000000000000001/10^18
			var exercisedBid *big.Int
			if winningSide {
				exercisedBid = longTokens
			} else {
				exercisedBid = shortTokens
			}
			expectedCreatorProfit := big.NewInt(0).Div(big.NewInt(0).Mul(exercisedBid, exerciseFeeMultiplier), tenTo18)
			creatorProfit, err := m.GetCreatorEarnings(*rpcURL, market)
			if err != nil {
				t.Errorf("test setup: getting option tokens: %v", err)
				t.FailNow()
			}
			if expectedCreatorProfit.Cmp(creatorProfit) != 0 {
				t.Errorf("expectedCreatorProfit: %v, creatorProfit: %v", expectedCreatorProfit, creatorProfit)
				t.FailNow()
			}

			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
				t.FailNow()
			}
		})
	}
}

func TestExerciseNotDecided(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	initialLong := "12300000000000000000"
	initialShort := "14500000000000000000"
	minCapital := "26800000000000000000"
	skewLimit := "80000000000000000000" // fraction, used as x/10^20
	bid := "1"
	side := true
	longTokens := "2"
	shortTokens := "0"
	var harbingerPrice int64 = 43272264136
	var tests = []struct {
		name             string
		biddingEndOffset int
		tradingEndOffset int
		want             error
	}{
		{"not decided", 20, 21, fmt.Errorf("winning side not decided yet")},
		// it's not possible to receive prices before trading ends, so attempts to exercise will get a "not decided yet"
		// caller isn't options contract: fail with "only options contract can call this entrypoint"
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			initialLong, succ := big.NewInt(0).SetString(initialLong, 10)
			if !succ {
				t.Errorf("test setup: initialLong: creating BigInt")
				t.FailNow()
			}
			initialShort, succ := big.NewInt(0).SetString(initialShort, 10)
			if !succ {
				t.Errorf("test setup: initialShort: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Second * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Second * time.Duration(test.tradingEndOffset)).Unix()
			biddingTicker := time.NewTimer(time.Second * (1 + time.Duration(test.biddingEndOffset)))
			tradingTicker := time.NewTimer(time.Second * (1 + time.Duration(test.tradingEndOffset)))

			// var price int64 = 43272264136
			harbinger, err := m.OriginateHarbinger(*rpcURL, *alice, "harbinger", *harbingerCode, harbingerPrice, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying harbinger: %v", err)
				t.FailNow()
			}
			marketFactory, err := m.OriginateMarketFactory(*rpcURL, *alice, "marketFactory", *marketFactoryCode, kUSD, harbinger, optionsFactory)
			if err != nil {
				t.Errorf("test setup: deploying market factory: %v", err)
				t.FailNow()
			}
			marketDeployer, err := m.OriginateMarketDeployer(*rpcURL, *alice, "marketDeployer", *marketDeployerCode, marketFactory, kUSD)
			if err != nil {
				t.Errorf("test setup: deploying market deployer: %v", err)
				t.FailNow()
			}
			market, options, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, initialLong, initialShort, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, "BTC-USD", 36000000000, biddingEnd, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			testBid, succ := big.NewInt(0).SetString(bid, 10)
			if !succ {
				t.Errorf("test setup: testBid: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Bid(*rpcURL, *bob, market, testBid, side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: bidding: %v", err)
				t.FailNow()
			}

			// check that new bid value is good
			newBids, err := m.GetBidsMapID(*rpcURL, market, side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid, err := m.GetBigMapElem(*rpcURL, *bob, newBids)
			if err != nil {
				t.Errorf("test setup: getting new bid: %v", err)
				t.FailNow()
			}
			if newBid.Cmp(testBid) != 0 {
				t.Errorf("test setup: got %v but bid %v", newBid, bid)
			}

			// wait for bidding to end
			<-biddingTicker.C

			// kUSD mock doesn't require approves for now
			err = m.Claim(*rpcURL, *alice, market, *bob)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: claiming: %v", err)
				t.FailNow()
			}

			// wait for claim to make it into a block
			time.Sleep(time.Second * 1)

			// check that new bid value is good
			newBids2, err := m.GetBidsMapID(*rpcURL, market, side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid2, err := m.GetBigMapElem(*rpcURL, *bob, newBids2)
			if err != nil {
				// bid has been claimed so we expect to find no value
				if !strings.Contains(err.Error(), fmt.Sprintf("no value for %s", *bob)) {
					t.Errorf("test setup: getting new bid: %v", err)
				}
			}
			result := big.NewInt(0)
			if newBid2.Cmp(result) != 0 {
				t.Errorf("newBid2: %v, wanted: %v", newBid2, result)
			}

			// get options bigmap, get elem for bob, check value
			optionMap, err := m.GetOptionsMapID(*rpcURL, options)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			longOptions, shortOptions, err := m.GetOptionsElem(*rpcURL, *bob, optionMap)
			if err != nil {
				t.Errorf("test setup: getting option tokens: %v", err)
				t.FailNow()
			}

			var longBid *big.Int
			var shortBid *big.Int
			if side {
				longBid = testBid
				shortBid = big.NewInt(0)
			} else {
				longBid = big.NewInt(0)
				shortBid = testBid
			}

			// longPrice = totalLong * 10^18 / total
			// longPrice set to 1 if 0
			// userTokens = userBid * 10^18 / longPrice

			tenTo18 := big.NewInt(0).Exp(big.NewInt(10), big.NewInt(18), nil)
			totalLong := big.NewInt(0).Add(initialLong, longBid)
			totalShort := big.NewInt(0).Add(initialShort, shortBid)
			total := big.NewInt(0).Add(totalLong, totalShort)
			longPrice := big.NewInt(0).Div(big.NewInt(0).Mul(totalLong, tenTo18), total)
			if longPrice.Cmp(big.NewInt(0)) == 0 {
				longPrice = big.NewInt(1)
			}
			longTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(longBid, tenTo18), longPrice)
			// short price = 1 - long price (but Michelson only has integers)
			// shortPrice = 10^18 - longPrice
			shortPrice := big.NewInt(0).Sub(tenTo18, longPrice)
			shortTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(shortBid, tenTo18), shortPrice)

			longTokens, succ := big.NewInt(0).SetString(longTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}
			shortTokens, succ := big.NewInt(0).SetString(shortTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}

			// TODO choose one of these ways to check
			// redoing the math requires rewriting when we change the contract math
			// the other one requires doing the math ahead of time and hardcoding results

			// check claimed options by redoing the math
			if longTokens2.Cmp(longTokens) != 0 {
				t.Errorf("longTokens2: %v, longTokens: %v", longTokens2, longTokens)
			}
			if shortTokens2.Cmp(shortTokens) != 0 {
				t.Errorf("shortTokens2: %v, shortTokens: %v", shortTokens2, shortTokens)
			}

			// check claimed options against our expected values
			if longOptions.Cmp(longTokens) != 0 {
				t.Errorf("longOptions: %v, wanted: %v", longOptions, longTokens)
			}
			if shortOptions.Cmp(shortTokens) != 0 {
				t.Errorf("shortOptions: %v, wanted: %v", shortOptions, shortTokens)
			}

			// wait for trading end and exercise
			<-tradingTicker.C

			// exercise by calling get_balance in options, specify %exercise in market
			// we can specify any number of users and which tokens we are interested in (long or short)
			// however the losing tokens are simply ignored so there is no point
			// TODO tests where we call the other side (losing side tokens should be ignored, nothing redemeed)
			_, err = m.Exercise(*rpcURL, *alice, market, options, kUSD, []string{*bob}, true)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: exercising: %v", err)
				t.FailNow()
			}

			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
			}
		})
	}
}

func TestWithdrawProfit(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	initialLong := "12300000000000000000"
	initialShort := "14500000000000000000"
	minCapital := "26800000000000000000"
	skewLimit := "80000000000000000000" // fraction, used as x/10^20
	var tests = []struct {
		name             string
		bid              string
		side             bool
		longTokens       string
		shortTokens      string
		harbingerPrice   int64
		wantKUSD         string
		biddingEndOffset int
		tradingEndOffset int
		want             error
	}{
		{"exercise 22 long won", "2200000000000000000", true, "4400000000000000000", "0", 43272264136, "4386800000000000000", 20, 21, nil},
		{"exercise 22 long lost", "2200000000000000000", true, "4400000000000000000", "0", 23272264136, "0", 20, 21, nil},
		{"exercise 22 short lost", "2200000000000000000", false, "0", "3820359281437125744", 43272264136, "0", 20, 21, nil},
		{"exercise 22 short won", "2200000000000000000", false, "0", "3820359281437125744", 23272264136, "3808898203592814367", 20, 21, nil},
		{"exercise tiny long won", "1", true, "2", "0", 43272264136, "2", 20, 21, nil},
		{"exercise tiny long lost", "1", true, "2", "0", 23272264136, "0", 20, 21, nil},
		{"exercise tiny short lost", "1", false, "0", "1", 43272264136, "0", 20, 21, nil},
		{"exercise tiny short won", "1", false, "0", "1", 23272264136, "1", 20, 21, nil},
		{"exercise huge long won", "123456000000000000000000000000000000000000000000000000000000", true, "123456000000000000123456000000000000123456000000000000123456", "0", 43272264136, "123085632000000000123085632000000000123085632000000000123086", 20, 21, nil},
		{"exercise huge long lost", "123456000000000000000000000000000000000000000000000000000000", true, "123456000000000000123456000000000000123456000000000000123456", "0", 23272264136, "0", 20, 21, nil},
		{"exercise huge short lost", "123456000000000000000000000000000000000000000000000000000000", false, "0", "123456000000000000123456000000000000123456000000000000123456", 43272264136, "0", 20, 21, nil},
		{"exercise huge short won", "123456000000000000000000000000000000000000000000000000000000", false, "0", "123456000000000000123456000000000000123456000000000000123456", 23272264136, "123085632000000000123085632000000000123085632000000000123086", 20, 21, nil},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			initialLong, succ := big.NewInt(0).SetString(initialLong, 10)
			if !succ {
				t.Errorf("test setup: initialLong: creating BigInt")
				t.FailNow()
			}
			initialShort, succ := big.NewInt(0).SetString(initialShort, 10)
			if !succ {
				t.Errorf("test setup: initialShort: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Second * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Second * time.Duration(test.tradingEndOffset)).Unix()
			biddingTicker := time.NewTimer(time.Second * (1 + time.Duration(test.biddingEndOffset)))
			tradingTicker := time.NewTimer(time.Second * (1 + time.Duration(test.tradingEndOffset)))

			harbinger, err := m.OriginateHarbinger(*rpcURL, *alice, "harbinger", *harbingerCode, test.harbingerPrice, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying harbinger: %v", err)
				t.FailNow()
			}
			marketFactory, err := m.OriginateMarketFactory(*rpcURL, *alice, "marketFactory", *marketFactoryCode, kUSD, harbinger, optionsFactory)
			if err != nil {
				t.Errorf("test setup: deploying market factory: %v", err)
				t.FailNow()
			}
			marketDeployer, err := m.OriginateMarketDeployer(*rpcURL, *alice, "marketDeployer", *marketDeployerCode, marketFactory, kUSD)
			if err != nil {
				t.Errorf("test setup: deploying market deployer: %v", err)
				t.FailNow()
			}

			market, options, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, initialLong, initialShort, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, "BTC-USD", 36000000000, biddingEnd, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			testBid, succ := big.NewInt(0).SetString(test.bid, 10)
			if !succ {
				t.Errorf("test setup: testBid: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Bid(*rpcURL, *bob, market, testBid, test.side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: bidding: %v", err)
				t.FailNow()
			}

			// check that new bid value is good
			newBids, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid, err := m.GetBigMapElem(*rpcURL, *bob, newBids)
			if err != nil {
				t.Errorf("test setup: getting new bid: %v", err)
				t.FailNow()
			}
			if newBid.Cmp(testBid) != 0 {
				t.Errorf("test setup: got %v but bid %v", newBid, test.bid)
			}

			// wait for bidding to end
			<-biddingTicker.C

			// kUSD mock doesn't require approves for now
			err = m.Claim(*rpcURL, *alice, market, *bob)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: claiming: %v", err)
				t.FailNow()
			}

			// wait for claim to make it into a block
			time.Sleep(time.Second * 1)

			// check that new bid value is good
			newBids2, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid2, err := m.GetBigMapElem(*rpcURL, *bob, newBids2)
			if err != nil {
				// bid has been claimed so we expect to find no value
				if !strings.Contains(err.Error(), fmt.Sprintf("no value for %s", *bob)) {
					t.Errorf("test setup: getting new bid: %v", err)
				}
			}
			result := big.NewInt(0)
			if newBid2.Cmp(result) != 0 {
				t.Errorf("newBid2: %v, wanted: %v", newBid2, result)
			}

			// get options bigmap, get elem for bob, check value
			optionMap, err := m.GetOptionsMapID(*rpcURL, options)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			longOptions, shortOptions, err := m.GetOptionsElem(*rpcURL, *bob, optionMap)
			if err != nil {
				t.Errorf("test setup: getting option tokens: %v", err)
				t.FailNow()
			}

			var longBid *big.Int
			var shortBid *big.Int
			if test.side {
				longBid = testBid
				shortBid = big.NewInt(0)
			} else {
				longBid = big.NewInt(0)
				shortBid = testBid
			}

			// longPrice = totalLong * 10^18 / total
			// longPrice set to 1 if 0
			// userTokens = userBid * 10^18 / longPrice

			tenTo18 := big.NewInt(0).Exp(big.NewInt(10), big.NewInt(18), nil)
			totalLong := big.NewInt(0).Add(initialLong, longBid)
			totalShort := big.NewInt(0).Add(initialShort, shortBid)
			total := big.NewInt(0).Add(totalLong, totalShort)
			longPrice := big.NewInt(0).Div(big.NewInt(0).Mul(totalLong, tenTo18), total)
			if longPrice.Cmp(big.NewInt(0)) == 0 {
				longPrice = big.NewInt(1)
			}
			longTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(longBid, tenTo18), longPrice)
			// short price = 1 - long price (but Michelson only has integers)
			// shortPrice = 10^18 - longPrice
			shortPrice := big.NewInt(0).Sub(tenTo18, longPrice)
			shortTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(shortBid, tenTo18), shortPrice)

			longTokens, succ := big.NewInt(0).SetString(test.longTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}
			shortTokens, succ := big.NewInt(0).SetString(test.shortTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}

			// TODO choose one of these ways to check
			// redoing the math requires rewriting when we change the contract math
			// the other one requires doing the math ahead of time and hardcoding results

			// check claimed options by redoing the math
			if longTokens2.Cmp(longTokens) != 0 {
				t.Errorf("longTokens2: %v, longTokens: %v", longTokens2, longTokens)
			}
			if shortTokens2.Cmp(shortTokens) != 0 {
				t.Errorf("shortTokens2: %v, shortTokens: %v", shortTokens2, shortTokens)
			}

			// check claimed options against our expected values
			if longOptions.Cmp(longTokens) != 0 {
				t.Errorf("longOptions: %v, wanted: %v", longOptions, longTokens)
			}
			if shortOptions.Cmp(shortTokens) != 0 {
				t.Errorf("shortOptions: %v, wanted: %v", shortOptions, shortTokens)
			}

			// wait for trading end and exercise
			<-tradingTicker.C

			// call %get in harbinger, specify asset string and %receive_prices of market
			err = m.GetPrices(*rpcURL, *alice, market, "BTC-USD", harbinger)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: getting prices: %v", err)
				t.FailNow()
			}

			winningSide, err := m.GetWinningSide(*rpcURL, market)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: getting winning side: %v", err)
				t.FailNow()
			}

			// exercise by calling get_balance in options, specify %exercise in market
			// we can specify any number of users and which tokens we are interested in (long or short)
			// however the losing tokens are simply ignored so there is no point
			// TODO tests where we call the other side (losing side tokens should be ignored, nothing redemeed)
			paidKUSD, err := m.Exercise(*rpcURL, *alice, market, options, kUSD, []string{*bob}, winningSide)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: exercising: %v", err)
				t.FailNow()
			}

			// math to calculate expected paidKUSD
			// tokens             (exercise_fee_multiplier     tokens    ten to 18)
			// 4400000000000000001-3000000000000000*4400000000000000001/10^18

			// check redeemed kUSD is correct
			wantKUSD, succ := big.NewInt(0).SetString(test.wantKUSD, 10)
			if !succ {
				t.Errorf("test setup: wantKUSD: creating BigInt")
				t.FailNow()
			}
			if paidKUSD.Cmp(wantKUSD) != 0 {
				t.Errorf("paidKUSD: %v, wantKUSD: %v", paidKUSD, wantKUSD)
				t.FailNow()
			}

			// get options for user, winning options should have been burned, losing are untouched
			longOptions2, shortOptions2, err := m.GetOptionsElem(*rpcURL, *bob, optionMap)
			if err != nil {
				t.Errorf("test setup: getting option tokens: %v", err)
				t.FailNow()
			}
			if winningSide {
				longOptions = big.NewInt(0)
			} else {
				shortOptions = big.NewInt(0)
			}
			if longOptions2.Cmp(longOptions) != 0 {
				t.Errorf("longOptions2: %v, longOptions: %v", longOptions2, longOptions)
				t.FailNow()
			}
			if shortOptions2.Cmp(shortOptions) != 0 {
				t.Errorf("shortOptions2: %v, shortOptions: %v", shortOptions2, shortOptions)
				t.FailNow()
			}

			// check creator profit
			// math to calculate expected creator profit
			// exercise_fee_multiplier     tokens    ten to 18
			// 3000000000000000*4400000000000000001/10^18
			var exercisedBid *big.Int
			if winningSide {
				exercisedBid = longTokens
			} else {
				exercisedBid = shortTokens
			}
			expectedCreatorProfit := big.NewInt(0).Div(big.NewInt(0).Mul(exercisedBid, exerciseFeeMultiplier), tenTo18)
			creatorProfit, err := m.GetCreatorEarnings(*rpcURL, market)
			if err != nil {
				t.Errorf("test setup: getting option tokens: %v", err)
				t.FailNow()
			}
			if expectedCreatorProfit.Cmp(creatorProfit) != 0 {
				t.Errorf("expectedCreatorProfit: %v, creatorProfit: %v", expectedCreatorProfit, creatorProfit)
				t.FailNow()
			}

			withdrawnProfit, err := m.WithdrawProfit(*rpcURL, *alice, market, kUSD)
			if err != nil {
				t.Errorf("test setup: getting option tokens: %v", err)
				t.FailNow()
			}
			if withdrawnProfit.Cmp(creatorProfit) != 0 {
				t.Errorf("withdrawnProfit: %v, creatorProfit: %v", withdrawnProfit, creatorProfit)
				t.FailNow()
			}

			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
				t.FailNow()
			}
		})
	}
}

func TestOptionsTiming(t *testing.T) {
	cancelFeeMultiplier, succ := big.NewInt(0).SetString("6000000000000000", 10)
	if !succ {
		t.Errorf("test setup: cancelFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	exerciseFeeMultiplier, succ := big.NewInt(0).SetString("3000000000000000", 10)
	if !succ {
		t.Errorf("test setup: exerciseFeeMultiplier: creating BigInt")
		t.FailNow()
	}
	initialLong := "12300000000000000000"
	initialShort := "14500000000000000000"
	minCapital := "26800000000000000000"
	skewLimit := "80000000000000000000" // fraction, used as x/10^20
	var tests = []struct {
		name             string
		bid              string
		side             bool
		longTokens       string
		shortTokens      string
		biddingEndOffset int
		tradingEndOffset int
		want             error
	}{
		{"move options before trading end long", "2200000000000000000", true, "4400000000000000000", "0", 9, 30, nil},
		{"move options before trading end short", "2200000000000000000", false, "0", "3820359281437125744", 9, 30, nil},
		{"move options after trading end long", "2200000000000000000", true, "4400000000000000000", "0", 9, 30, fmt.Errorf("trading phase over")},
		{"move options after trading end short", "2200000000000000000", false, "0", "3820359281437125744", 9, 30, fmt.Errorf("trading phase over")},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			initialLong, succ := big.NewInt(0).SetString(initialLong, 10)
			if !succ {
				t.Errorf("test setup: initialLong: creating BigInt")
				t.FailNow()
			}
			initialShort, succ := big.NewInt(0).SetString(initialShort, 10)
			if !succ {
				t.Errorf("test setup: initialShort: creating BigInt")
				t.FailNow()
			}
			minCapital, succ := big.NewInt(0).SetString(minCapital, 10)
			if !succ {
				t.Errorf("test setup: minCapital: creating BigInt")
				t.FailNow()
			}
			skewLimit, succ := big.NewInt(0).SetString(skewLimit, 10)
			if !succ {
				t.Errorf("test setup: skewLimit: creating BigInt")
				t.FailNow()
			}
			biddingEnd := time.Now().Add(time.Second * time.Duration(test.biddingEndOffset)).Unix()
			tradingEnd := time.Now().Add(time.Second * time.Duration(test.tradingEndOffset)).Unix()
			biddingTicker := time.NewTimer(time.Second * (1 + time.Duration(test.biddingEndOffset)))
			tradingTicker := time.NewTimer(time.Second * (1 + time.Duration(test.tradingEndOffset)))
			// var price int64 = 43272264136
			// harbinger, err := m.OriginateHarbinger(*rpcURL, *alice, "harbinger", *harbingerCode, price, tradingEnd)
			// if err != nil {
			// 	t.Errorf("test setup: deploying harbinger: %v", err)
			// 	t.FailNow()
			// }
			market, fa2, err := m.Deploy(*rpcURL, *alice, marketDeployer, kUSD, initialLong, initialShort, cancelFeeMultiplier, exerciseFeeMultiplier, minCapital, skewLimit, "BTC-USD", 36000000000, biddingEnd, tradingEnd)
			if err != nil {
				t.Errorf("test setup: deploying market: %v", err)
				t.FailNow()
			}
			testBid, succ := big.NewInt(0).SetString(test.bid, 10)
			if !succ {
				t.Errorf("test setup: testBid: creating BigInt")
				t.FailNow()
			}
			// kUSD mock doesn't require approves for now
			err = m.Bid(*rpcURL, *bob, market, testBid, test.side)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: bidding: %v", err)
				t.FailNow()
			}

			// check that new bid value is good
			newBids, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid, err := m.GetBigMapElem(*rpcURL, *bob, newBids)
			if err != nil {
				t.Errorf("test setup: getting new bid: %v", err)
				t.FailNow()
			}
			if newBid.Cmp(testBid) != 0 {
				t.Errorf("test setup: got %v but bid %v", newBid, test.bid)
			}

			// wait for bidding to end
			<-biddingTicker.C

			// kUSD mock doesn't require approves for now
			err = m.Claim(*rpcURL, *alice, market, *bob)
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: claiming: %v", err)
				t.FailNow()
			}

			// wait for claim to make it into a block
			time.Sleep(time.Second * 1)

			// check that new bid value is good
			newBids2, err := m.GetBidsMapID(*rpcURL, market, test.side)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			newBid2, err := m.GetBigMapElem(*rpcURL, *bob, newBids2)
			if err != nil {
				// bid has been claimed so we expect to find no value
				if !strings.Contains(err.Error(), fmt.Sprintf("no value for %s", *bob)) {
					t.Errorf("test setup: getting new bid: %v", err)
				}
			}
			result := big.NewInt(0)
			if newBid2.Cmp(result) != 0 {
				t.Errorf("newBid2: %v, wanted: %v", newBid2, result)
			}

			// get options bigmap, get elem for bob, check value
			optionMap, err := m.GetOptionsMapID(*rpcURL, fa2)
			if err != nil {
				t.Errorf("test setup: getting map ID: %v", err)
				t.FailNow()
			}
			longOptions, shortOptions, err := m.GetOptionsElem(*rpcURL, *bob, optionMap)
			if err != nil {
				t.Errorf("test setup: getting option tokens: %v", err)
				t.FailNow()
			}

			var longBid *big.Int
			var shortBid *big.Int
			if test.side {
				longBid = testBid
				shortBid = big.NewInt(0)
			} else {
				longBid = big.NewInt(0)
				shortBid = testBid
			}

			// longPrice = totalLong * 10^18 / total
			// userTokens = userBid * 10^18 / longPrice

			tenTo18 := big.NewInt(0).Exp(big.NewInt(10), big.NewInt(18), nil)
			totalLong := big.NewInt(0).Add(initialLong, longBid)
			totalShort := big.NewInt(0).Add(initialShort, shortBid)
			total := big.NewInt(0).Add(totalLong, totalShort)
			longPrice := big.NewInt(0).Div(big.NewInt(0).Mul(totalLong, tenTo18), total)
			if longPrice.Cmp(big.NewInt(0)) == 0 {
				longPrice = big.NewInt(1)
			}
			longTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(longBid, tenTo18), longPrice)
			// short price = 1 - long price (but Michelson only has integers)
			// shortPrice = 10^18 - longPrice
			shortPrice := big.NewInt(0).Sub(tenTo18, longPrice)
			shortTokens2 := big.NewInt(0).Div(big.NewInt(0).Mul(shortBid, tenTo18), shortPrice)

			longTokens, succ := big.NewInt(0).SetString(test.longTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}
			shortTokens, succ := big.NewInt(0).SetString(test.shortTokens, 10)
			if !succ {
				t.Errorf("test setup: tokens: creating BigInt")
				t.FailNow()
			}

			// TODO choose one of these ways to check
			// redoing the math requires rewriting when we change the contract math
			// the other one requires doing the math ahead of time and hardcoding results

			// check claimed options by redoing the math
			if longTokens2.Cmp(longTokens) != 0 {
				t.Errorf("longTokens2: %v, longTokens: %v", longTokens2, longTokens)
			}
			if shortTokens2.Cmp(shortTokens) != 0 {
				t.Errorf("shortTokens2: %v, shortTokens: %v", shortTokens2, shortTokens)
			}

			// check claimed options against our expected values
			if longOptions.Cmp(longTokens) != 0 {
				t.Errorf("longOptions: %v, wanted: %v", longOptions, longTokens)
			}
			if shortOptions.Cmp(shortTokens) != 0 {
				t.Errorf("shortOptions: %v, wanted: %v", shortOptions, shortTokens)
			}

			// wait for trading to end?
			if test.want != nil {
				<-tradingTicker.C
			}

			var tokenID int
			if test.side {
				tokenID = 0
			} else {
				tokenID = 1
			}

			// try to move options
			err = m.TransferOptions(*rpcURL, *bob, *alice, fa2, tokenID, big.NewInt(1))
			if err != nil {
				if test.want != nil {
					if strings.Contains(err.Error(), test.want.Error()) {
						// error same as expected means the test passed
						return
					}
				}
				t.Errorf("test setup: claiming: %v", err)
				t.FailNow()
			}

			if test.want != nil {
				t.Errorf("expected contract error: %v got: %v", test.want, "success")
			}
		})
	}
}
