module gitlab.com/smartcontractlabs/sexp-binary-options

go 1.16

require (
	github.com/bitfield/script v0.18.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/savaki/jq v0.0.0-20161209013833-0e6baecebbf8
)
