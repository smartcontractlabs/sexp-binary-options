package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/smartcontractlabs/sexp-binary-options/d"
	m "gitlab.com/smartcontractlabs/sexp-binary-options/market"
)

var creator = flag.String("creator", "tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1", "address of the market creator, tezos-client needs to have the keys")
var harbinger = flag.String("harbinger", "KT1SUP27JhX24Kvr11oUdWswk7FnCW78ZyUn", "Harbinger address")
var kUSD = flag.String("kusd", "KT1LWN8m9JMAzeV4nyfzpVigchr84NWGQbUw", "kUSD address")
var rpcURL = flag.String("rpcurl", "https://florencenet.smartpy.io/", "url of tezos-node rpc")

var originateDeployer = flag.Bool("originateDeployer", false, "use to originate the deployer contract instead")
var marketDeployerCode = flag.String("marketDeployerCode", "../market/contracts/deployer/market_deployer.tz", "filename of market contract")
var marketFactoryCode = flag.String("marketFactoryCode", "../market/contracts/deployer/market_factory.tz", "filename of market contract")
var optionsFactoryCode = flag.String("optionsFactoryCode", "../market/contracts/deployer/ledger_factory.tz", "filename of options FA2 contract")

var debug = flag.Bool("debug", false, "print debug information")
var verbose = flag.Bool("verbose", false, "print more information")

func main() {
	flag.Parse()
	d.Debug = *debug
	d.Verbose = *verbose

	d.Debugln("creator:", *creator)
	d.Debugln("harbinger:", *harbinger)
	d.Debugln("kUSD:", *kUSD)
	d.Debugln("rpcURL:", *rpcURL)

	// check for tezos-client
	if _, err := os.Stat("tezos-client"); os.IsNotExist(err) {
		fmt.Println("The tezos-client binary is required to run the deploy script.")
		fmt.Println("If you don't have it, you can get it from the serokell repo.")
		fmt.Println(`$ wget https://github.com/serokell/tezos-packaging/releases/latest/download/tezos-client`)
		fmt.Println(`$ chmod +x tezos-client`)
		fmt.Println("If you already have the tezos-client binary, you can copy it into this folder.")
		return
	}

	optionsFactory, err := m.OriginateOptionsFactory(*rpcURL, *creator, `options_factory_prod`, *optionsFactoryCode)
	if err != nil {
		log.Fatalf("originating options factory: %v", err)
	}

	marketFactory, err := m.OriginateMarketFactory(*rpcURL, *creator, `market_factory_prod`, *marketFactoryCode, *kUSD, *harbinger, optionsFactory)
	if err != nil {
		log.Fatalf("originating market factory: %v", err)
	}

	marketDeployer, err := m.OriginateMarketDeployer(*rpcURL, *creator, `market_deployer_prod`, *marketDeployerCode, marketFactory, *kUSD)
	if err != nil {
		log.Fatalf("originating market deployer: %v", err)
	}

	fmt.Println("The market deployer contract and the factory contracts have been deployed.")
	fmt.Println("Market factory contract address:", marketFactory)
	fmt.Println("Options factory contract address:", optionsFactory)
	fmt.Println("Market deployer contract address:", marketDeployer)
}
