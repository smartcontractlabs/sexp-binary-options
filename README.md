# SEXP Binary Options

SEXP Binary Options is a DeFi product allowing anyone to get exposure to binary outcomes involving crypto assets. We plan to add other asset types in the future. Underlying asset data is provided by the [Harbinger oracle](https://github.com/tacoinfra/harbinger).

The system consists of several smart contracts, servers that interact with the smart contracts, and a web-app that presents a streamlined user interace.

In this repo you can find the smart contracts, tools for automated testing of the smart contracts, tools for automated deployment, a metadata server, and also source code for a watchtower server that improves UX by automating certain actions.

## Product overview

This product lets users take long or short positions on future prices of crypto assets. At maturity, the options are resolved into a binary outcome (win or lose). 1 option is always resolved either into 1 kUSD or it becomes worthless. Options have 18 decimal precision, just like kUSD. Prices of the underlying assets are provided by the Harbinger oracle.

When using the product, users interact with markets. Each market is concerned with the price of one asset, at a specified date. The asset, target price and the date when options mature are specified when the market is created. Anyone can become a market creator and create their own market. Market creators provide initial capital to improve price discovery and earn fees from people taking positions through the market.

From market creation to resolution, the market progresses through the following phases:

0. creation
1. bidding
2. trading
3. resolved

At the beginning the market is created & initialized by the market deployer. Then the market is open for bidding. Users can bid on long or short outcome and also cancel their bids if they wish so. Cancellation of bids can be penalized by a cancellation fee. After bidding is over, prices for long and short options are calculated and users can redeem their options. Both long and short options are represented by FA2 tokens in a separate contract (each market has its own options contract). The FA2 option tokens are freely transferrable and users can e.g. trade them to 3rd parties. After the market is resolved, the winning side tokens can be redeemed for kUSD. The tokens for the losing side become worthless.

To bid and cancel bids, users have to call the `bid` and `cancel` entrypoints of the market contract. This can be done either via the companion web-app or manually through an explorer such as Better Call Dev or even via command line.

The market also has entrypoints such as `claim`, `exercise` and `receive_prices`, however in most cases the users shouldn't have to worry about these. How is this possible? We have designed our contracts so that some of the entrypoints can be called by anyone. Most of the time our watchtower will do this. A watchtower is a robot that monitors the blockchain and triggers actions as needed. This results in improved UX without security compromises. The community can help by running watchtowers too, to reduce the risk of outages. Remember, in cases where the watchtower is offline or broken for any reason, any user can call these entrypoints manually to redeem their options or kUSD as needed.

## How the contracts work

The system consists of a market contract, and an options contract. The options contract is an FA2 contract that contains a ledger with balances of option tokens (long and short tokens).

### Market contract

The market contract has the following entrypoints:
1. bid
2. cancel
3. claim
4. exercise
5. receive_ledger
6. receive_prices
7. withdraw_profit
8. init

#### bid

Users call the bid entrypoint, specifying how much kUSD they want to bid and whether to bid on a long or short outcome (true for long and false for short).

#### cancel

Users call the cancel entrypoint, specifying how much kUSD of their existing bid they want to cancel, and whether to cancel a long bid or short bid (true for long and false for short).

#### claim

Anyone can call the claim enrypoint after the bidding phase ends and specify a list of addresses. Bids for the addresses are erased and tokens representing long and/or short options are minted for that address in the companion FA2 contract.

#### exercise

After the trading phase ends, anyone can call the get_balance entrypoint of the companion FA2 contract, and specify as a callback address the exercise entrypoint of the market contract. The market receives the balances and exchanges the winning tokens for kUSD. In other words, either the long or short tokens are burned and corresponding amount of kUSD is paid out to the address that owned those tokens. Tokens for the losing outcome are simply ignored.

#### receive_prices

After the trading phase ends, and before exercise can be successfuly called, the market contract has to receive prices from the Harbinger oracle. Anyone can trigger this by calling the oracle's get entrypoint and specifying the receive_prices entrypoint of the market contract as the callback address.

#### receive_ledger

Used during market creation to receive and save the address of the options contract that holds the options ledger.
 
#### init

Used during market creation. The address of the market creator and the starting long and short bids are specified. Fresh big maps are created with the starting bids. The bids are checked for compliance with the creator limits (min_capital and skew_limit). The required kUSD is drawn from market creator by market factory contract (the contract that creates market contracts).

#### withdraw_profit

Allows the market creator to withdraw their portion of the profit. The profit consists of the exercise fees paid by users when redeeming kUSD for their winning options.

### Options contract

The options contract uses the FA2 standard and has the following entrypoints:
1. transfer
2. balance_of
3. update_operators

The entrypoints are described in the [FA2 standard](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md), however here's a short explanation.

#### transfer
Transfer is used for sending tokens around. Either the owner, or an address in the list of operators can call the transfer entrypoint to send money or draw money from an account that approved it as an operator.

#### balance_of
This is used to query the balance of some address or several addresses.

#### update_operators
A list of approved operators belongs to each address. An address can add or remove operators as needed and the operators are allowed to transfer funds on their behalf. There is no limit specified and the operator can use all of the funds of an address.

## How to run tests

You'll need the files in this repo, tezos-client binary and a functioning Tezos sandbox.

```
git clone https://gitlab.com/smartcontractlabs/sexp-binary-options.git
cd sexp-binary-options/market
```

If you don't have tezos-client you can get a recent pre-compiled binary from the serokell repo:

```
wget https://github.com/serokell/tezos-packaging/releases/latest/download/tezos-client
chmod +x tezos-client
```

The easiest way to run a Tezos sandbox is by using the [Docker images from TQ Tezos](https://assets.tqtezos.com/docs/setup/2-sandbox/).

If you don't have Docker yet, I recommend you [follow the instructions here to install the rootless version](https://docs.docker.com/engine/security/rootless/#distribution-specific-hint).

Run the Tezos Florencenet sandbox:

```
docker run --rm --name my-sandbox -e block_time=1 --detach -p 20000:20000 tqtezos/flextesa:20210316 flobox start
```

Notice we can set the block time here to 1 second. Otherwise the tests would take ages to run.

After you are done testing or if you need to restart the sandbox and start from scratch, you can turn off the sandbox with:

```
docker kill my-sandbox
```

Detailed instructions and more docs for running Tezos sandboxes are provided [here](https://tezos.gitlab.io/flextesa/).

Now you can finally run the tests.

Run go build to get the dependencies and build the binary.

```
go build
```

Run tests using the go command.

```
go test
```

Since the tests can take a while to run, you can use the verbose flag to see which test is currently running and some additional info.

```
go test -v
```

You can use the run option to select only some tests to run.

```
go test -v -run="ClaimTiming|Exercise"
```

When writing new tests or modifying existing ones, you might want to see debug information from the tests.

```
go test -v -debug
```

Sometimes the tests can fail with timing errors like `still in bidding phase`. You can try to restart the sandbox or fiddle with the timings.


## How to deploy the contracts and create markets

The system relies on factory contracts to create new markets. To deploy the factory contracts, clone the repo and navigate to the deployer folder
```
git clone https://gitlab.com/smartcontractlabs/sexp-binary-options.git
cd sexp-binary-options/deployer
```

Make sure there is a tezos-client binary in the folder with the watchtower.
If you don't have tezos-client you can get a pre-compiled binary from the serokell repo:

```
wget https://github.com/serokell/tezos-packaging/releases/latest/download/tezos-client
chmod +x tezos-client
```

Run go build to get the dependencies and build the binary.

```
go build
```

Run the binary. By default the contracts are deployed to the delphinet testing network.

```
./deployer
```

For more info about command line flags use the help flag.

```
./deployer -help
```

You can specify addressses of Harbinger and kUSD contracts. The program deploys factory contracts that are going to be used to create new markets.

When trying to track down a bug or understand why something doesn't work, it might be useful to turn on the debug logs.

```
./deployer -debug
```

After the factory contracts are deployed, you can navigate to the top level of the repo and call the factory contracts to create new markets.

```
cd ..
go build
./sexp-binary-options -help
./sexp-binary-options -deployer KT123456789123456789123456789 -creator tz123123123123123123123123123
```

The program calls the specified deployer contract and uses it to create a new market. Various options can be specified, such as asset, target price, the end of bidding and trading phases, long and short bids of the creator, skew limit or minimum capital. The deployer creates two new contracts, a market contract and an FA2 options contract that contains option tokens. The deployer also draws the required funds for the initial bids.

The tezos-client needs to know the keys of the creator address.

## How to use the watchtower

The watchtower is a robot that helps improve the UX of SEXP Binary Options. It's written in Go and relies on the tezos-client binary and the Better Call Dev indexer API. It keeps track of the binary options smart contracts and triggers some actions so that users don't have to. It's meant to run for long periods of time.

Clone the repo and navigate to the watchtower folder.

```
git clone https://gitlab.com/smartcontractlabs/sexp-binary-options.git
cd sexp-binary-options/watchtower
```

Make sure there is a tezos-client binary in the folder with the watchtower.
If you don't have tezos-client you can get a pre-compiled binary from the serokell repo:

```
wget https://github.com/serokell/tezos-packaging/releases/latest/download/tezos-client
chmod +x tezos-client
```

Run go build to get the dependencies and build the watchtower.

```
go build
```

Run it:

```
./watchtower
```

To get more info about command line options:

```
./watchtower -help
```