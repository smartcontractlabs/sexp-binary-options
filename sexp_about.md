# About SEXP

SEXP is a Tezos based decentralized exchange for synthetic assets.

## Overview

Our Binary Options product lets traders bet on the price development of a wide range of underlying cryptocurrencies. We are working on a new swap product with swaps between arbitrary pairs of assets and no slippage. Imagine if every asset on Uniswap had a pair with every other asset & no liquidity problems.

## Binary Options

The system consists of individual markets that are concerned with a single outcome. Anyone can create a market and charge traders that use their market a fee. Users can compare different markets and choose a market with low fees and high enough volume.

The market progresses through phases, starting with bidding phase. In the bidding phase, bids can be submitted for long and short option tokens. Bids can be canceled, and a cancel fee can be used to discourage order flashing.

The next phase after bidding is trading phase. Bids on both sides are used to calculate prices for long and short option tokens, and bids can be redeemed for the option tokens. The option tokens implement the FA2 standard and can be transferred using a wallet and traded on DEXes like Quipuswap or Dexter.

After trading phase the market can be resolved. The winning side is decided based on price data from the Harbinger oracle. Option tokens of the winning side can be redeemed for 1 kUSD each, while the losing side option tokens become worthless.

## How to Trade

You can try the product running on the Florence testnet on florencenet.sexp.exchange.

First you are going to need some Florencenet kUSD. You can either mint them on [zeronet.kolibri.finance](https://zeronet.kolibri.finance), buy some on Quipuswap or get them from a faucet.

On the index page you can see an overview of markets that can be sorted or filtered by asset. Clicking a row takes you to the detailed view of the market where you can bid or cancel bids in bidding phase. After bidding, you can redeem option tokens (to be added) or simply wait until our robot redeems on your behalf. After the market is resolved, you can similarly redeem options for kUSD (to be added) or wait for our robot to do it for you.

## How to Farm

Anyone can create a market and earn fees. The market creation screen has several parameters you can configure. The following section explains what each of the parameters means.

### long and short bids
These are your initial bids. They should be high enough to make the market attractive to traders, who are looking for sufficient capitalization (volume).

### minimum capital and skew limit
These limits restrict how much money the creator has to keep in the contract at all times, and what the maximum skew can be of the larger bid compared to both bids. These limits ensure that the market creator can't withdraw all of their funds or change the prices of options at the last moment.

The condition that has to be satisfied is `(larger_bid/both_bids) <= skew_limit`. So a skew limit of 80% (written as 0.8) means that if a bid or cancel by the creator would result in the larger bid making up more than 80% of the value of a market, the bid or cancel will fail.

### exercise fee and cancel fee
Cancel fee can be used to discourage canceling of bids. The fee is used to improve the prices of all remaining participants. Exercise fee is the source of the market creator's profit and is taken from the winning options when they are redeemed.

The fees are specified as a percentage, so that 0.003 means 0.3%.


## Assets Supported

We are using the [Harbinger oracle](https://github.com/tacoinfra/harbinger) to get asset prices, so we support the assets supported by Harbinger. You can find the list by inspecting the [storage of the Harbinger contracts](https://better-call.dev/mainnet/KT1AdbYiPYb5hDuEuVrfxmFehtnBCXv4Np7r/storage).

## SEXP Code

The code for SEXP Binary Options is available on Gitlab. The web app is in [https://gitlab.com/smartcontractlabs/sexp-webui](https://gitlab.com/smartcontractlabs/sexp-webui) and the contracts, tests, deployment logic and watchtower are in [https://gitlab.com/smartcontractlabs/sexp-binary-options](https://gitlab.com/smartcontractlabs/sexp-binary-options)


## Future Plans & Roadmap

Our roadmap is currently available at [sexp.exchange](https://sexp.exchange)

In addition to Binary Options, we are also working hard to ship our next product: Synthetic Swap.

The assets in Synthetic Swap are going to continuously mirror the performance of their underlying asset. Traders are going to be able to swap any synthetic asset for any other asset, without any liquidity or slippage problems. You can imagine it like all assets on Quipuswap/Uniswap having a well capitalized trading pair with each other.

Synthetic Swap is going to support a wide range of synthetic assets including inverted versions. Inverted versions of synthetic assets can be used to bet on the price going down.

Further in the future, we are also going to build stock market oracles and add synthetic assets that mirror traditional stocks.
