# Adding Support for SEXP to Wallets

SEXP Binary Options system consists of two contracts: a market contract where bidding takes place, and an options contract that contains a ledger with option token balances. The market contract also handles transforming bids to option tokens and option tokens to kUSD.

The ledger contract contains a placeholder link to our metadata server at [metadata.sexp.exchange](https://metadata.sexp.exchange). A placeholder metadata is served at this address, that looks like this:

```
{
      "name":"SEXP Synth Token",
      "symbol":"TOKEN",
      "decimals":"18"
}
```

The placeholder can be used by wallets that don't support SEXP specifically.

It is preferred that wallets add support for SEXP in the following way. When a SEXP Synth Token contract is encountered, fetch the metadata from off-chain storage at `https://metadata.sexp.exchange/<options contract address>`

Example usage:

```
curl https://metadata.sexp.exchange/KT1ACzN41BZk3trtSVNUgUvdyaZ7KXCpe6ZY
```

The metadata object returned looks like this:

```
{
   "0":{
      "name":"BTC-USD 100k 20th April 2021 Long",
      "symbol":"BTC100L",
      "decimals":"18"
   },
   "1":{
      "name":"BTC-USD 100k 20th April 2021 Short",
      "symbol":"BTC100S",
      "decimals":"18"
   }
}
```

The options contract contains tokens for both long and short options. The contract is intended to be mostly compliant with the [TZIP16](https://gitlab.com/tzip/tzip/blob/master/proposals/tzip-16/tzip-16.md), with the small change that the contract address has to be appended to the off-chain metadata URL specified in the contract. This is because the contract address is not known when the contract is created.